# ECO-WISE MERN STACK DEMO PLATFORME E_COMMERCE
![ecowiselogo](/uploads/94f6d4cd092e09030896f1d6c46e1cb4/ecowiselogo.png)

EcoWise est un exemple d'application qui montre un site Web de commerce utilisant "MERN STACK"(MongoDB,Express,ReactJs et Node JS) 

Le but du projet est de : 
- Mettre en évidence les techniques de création et de sécurisation d'une application complète REST à l'aide de NodeJS.
- Manipulation de la base de données MongoDB  à travers Mongoose .
- l'intégration de la bibliothèque Redux. 
- La Consommation de service RESTfull.
- Manipulation des "Hooks" (useState,useEffect,useParams...)

### Technologies Utiliser
Composant         | Technologie
---               | ---
Frontend          | [ReactJs](javascript JSX)
Backend (REST)    | [NodeJs/Express](javascript)
Securité          | [JsonWebToken] (JWT)
Centralisation    | [Redux]
DataBase          | [MongoDB]
ODM               | [Mongoose]
Désign            | [BootStrap,reactstrap]
Fetching DATA     | [Axios]


### Caractéristiques du projet
* Backend
  * Sécurité basée sur les jetons (à l'aide de Json Web Token) et leur actualisation.
  * Utilisation du Mongoose ODM (Object Document Mapping) pour Modéliser le schéma de base de données .
  * Implémentation RESTfull 
* Frontend
  * Organisation des composants, services, directives, pages, etc. dans une application React.
  * Centralisation des données à l'aide de la bibliothèque Redux.
  * Routage et protection des pages nécessitant une authentification
  * Basic visulaization
  * Consommation des services RESTfull

# Backend NodeJS/Express
Une application NodeJs/Express pour sécuriser une API REST avec un jeton Web JSON (JWT) qui nécessite qui nécessite une actualisation après une période de temps spécifique.
## controle d'accès
### JsonWebToken
* Utilisez le jeton pour accéder aux ressources via votre API RESTful.
* Accéder au contenu disponible pour tous les utilisateurs authentifiés.
``` javascript
const jwt = require('jsonwebtoken');
const accessTokenSecret = process.env.ACCESS_TOKEN_SECRET;
const authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;
    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }
            req.user = user;
            next();
        });
    } else {
        res.sendStatus(401);
    }
};
module.exports = authenticateJWT;

router.get('/Allproducts', authJwtHelper,async (req, res) => {....});
```
## Connexion MongoDB
Etablir la connection à la base de données
``` javascript
var mongoDB = process.env.DB_URL;
mongoose.connect(mongoDB, { useNewUrlParser: true , useUnifiedTopology: true, useFindAndModify: false });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', function (callback) {
  console.log("connection mongo db etablit");
});
```
## Routage

``` javascript
app.use('/users', usersRouter);
app.use('/products', productRouter);
app.use('/categories', categoryRouter);
app.use('/orders', orderRouter);
app.use('/orderDetails', orderDetailRouter);
```
# FrontEnd ReactJs
## Project Structure

```bash
├── src
|  ├── assets
|  ├── components
|  |  ├── Admin
|  |  ├── app
|  |  ├── common
|  |  ├── NavBar 
|  |  ├── authentification   
|  |  └── index.js
|  ├── container
|  |  ├── category 
|  |  ├── product   
|  |  └── user
|  ├── helpers
|  |  ├── axiosInterceptors.js   
|  |  └── history.js
|  ├── redux
|  |  ├── auth   
|  |  ├── category
|  |  ├── Order
|  |  ├── Product
|  |  ├── user
|  |  ├── actions.js
|  |  ├── reducers.js
|  |  └── store.js
|  ├── Services
|  |  ├── Auth   
|  |  ├── Category
|  |  ├── Order
|  |  ├── Product
|  |  └── User
|  ├── App.css
|  ├── App.js
|  ├── App.test.js
|  ├── index.css
|  ├── index.js
├── ...
```
### Routage 
```javascript
 <Router >
    <Switch>
      <AuthRoute  path="/app" auth={isLoggedIn} component={AppRoutes} />
      <Route  path="/authentication" component={UserRoutes} />
      <Route  path="/admin" component={AdminRoutes} />
      <Route path="/" exact component={Main} />
    </Switch>
  </Router>
  ...
  const AppRoutes = ({ match }) => {
    return (
        <MainLayout>
        <Switch>
            <Redirect exact from={`${match.url}/`} to={`${match.url}/product`} />
            <Route  path={`${match.url}/product`} component={Product} />
            <Route  path={`${match.url}/category`} component={Category} />
            <Route  path={`${match.url}/orderdetails/:idorder`} component={OrderDetails} />
            <Route  path={`${match.url}/productdetails/:idproduct`} component={Productdetails} />
            <Route  path={`${match.url}/cart`} component={CartComponent} />
            <Route  path={`${match.url}/orders`} component={OrderComponent} />
        </Switch>
        </MainLayout>
    );
};
export default (AppRoutes);
```
## protection des pages qui nécessitant une authentification  

```javascript

const AuthRoute = ({ component: Component, auth, ...rest }) => (
<div>
    {console.log(auth)}
    <Route
        {...rest}
        render={props =>
            auth ? (<Component {...props} />) : (<Redirect to={{ pathname: "/authentication/login"}} />)
        }
    />
</div>
);
...
<AuthRoute  path="/app" auth={isLoggedIn} component={AppRoutes} />
```
## Load Data
```javascript
  return await axios.get(`${apiUrl}/Allproducts`).then((response) => {
            return response.data;
        });
```
## Axios-Interceptor 

### Attach Authorization header for all axios requests
```javascript
axios.interceptors.request.use(
    config => {
        const token = localStorage.getItem('token');
        config.headers.authorization = `Bearer ${token}`;

        return config;
    },
    error => {
        return Promise.reject(error);
    },
);
```
### Axios interceptor to refresh JWT token after expiration

```javascript
axios.interceptors.response.use((response) => {
    return response
}, async function (error) {
    const originalRequest = error.config;
    if (error.response.status === 403 && !originalRequest._retry) {
        originalRequest._retry = true;
        const access_token = await getRefreshJwt();
        localStorage.setItem('token', access_token.accessToken);
        axios.defaults.headers.common['authorization'] = 'Bearer ' + access_token.accessToken;
        return axios(originalRequest);
    }
    return Promise.reject(error);
});
```
## Redux

### Actions

```javascript
export const GET_PRODUCTS_FAIL="GET_PRODUCTS_FAIL";
export const GET_PRODUCTS_SUCCESS="GET_PRODUCTS_SUCCESS";
export const ADD_PRODUCTS_SUCCESS="ADD_PRODUCTS_SUCCESS";
export const ADD_PRODUCTS_FAIL="ADD_PRODUCTS_FAIL";
export const DELETE_PRODUCTS_SUCCESS="DELETE_PRODUCTS_SUCCESS";
export const DELETE_PRODUCTS_FAIL="DELETE_PRODUCTS_FAIL";
...
```
### Reducers

```javascript
export default combineReducers({
    auth,
    message,
    user,
    product,
    category,
    order
});
```
### Store
```javascript
const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(...middleware))
);
export default store;
```
## ScreenShot
### Client
![Sign_in](/uploads/d2fb0c425877d8d9be3c65d3b816ab21/Sign_in.png)

![signup](/uploads/1b3cc2e4188a02fce8d20a55515405d6/signup.png)

![Client_Product_List](/uploads/e1bec81db476e382fb2c62b2ac74b6a5/Client_Product_List.png)

![Client_Product_Details](/uploads/1d7ae982f166a0819fe27302cae1f30b/Client_Product_Details.png)

![Client_Cart](/uploads/132e76fa581e94dab20403e6df93e79d/Client_Cart.png)

![Client_Orders](/uploads/af0f82efffbfc0e069a483ac3ff51386/Client_Orders.png)

![Client_Orders_Details](/uploads/2e570cadc533c42f152e63e05afd77bf/Client_Orders_Details.png)

![Client_Orders_Details_Print](/uploads/1e61fa071c8f2304dff500f72aa31896/Client_Orders_Details_Print.png)

### Admin

![Admin_categories](/uploads/c25c8092197fd7ad90b14b9433d84554/Admin_categories.png)

![Admin_Product](/uploads/70886bfb0d0758c07ec6fd7a6361c9a4/Admin_Product.png)

![Admin_users](/uploads/d1ca0f691d3341ae8f2eb666aa102945/Admin_users.png)










