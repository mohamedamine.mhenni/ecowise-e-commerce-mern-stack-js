var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var ProductSchema = new Schema({
    title:  {
        type:String,
        required: [true, 'title  invalid']
    },
    owner:{
        type: Schema.ObjectId, ref: 'User'
    },
    price:{
        type:Number,
        min:0.1,
        required: [true, 'price invalid']
    },
    description:{
        type:String,
        required:[true , 'description  obligtoire']
    },
    image:{type:String},
    pictures:[],
    category:{
        type: Schema.ObjectId, ref: 'Category'
    },
    createdDate:{
        type:Date,
        min:Date.now(),
        default:Date.now(),
        required:[true , 'date invalide']
    }
});
// Compile model from schema
var Product = mongoose.model('Product', ProductSchema );


module.exports = Product;
