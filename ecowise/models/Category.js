var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var CategorySchema = new Schema({
    title:  {
        type:String,
        required: [true, 'title  invalid']
    },
    parent: {
        type: Schema.ObjectId, ref: 'Category'
    },
    description:{
        type:String,
        required: [true,'Description Obligatoire']
    },
    createdDate:{
        type:Date,
        min:Date.now(),
        default:Date.now(),
        required:[true , 'date invalide']
    }
});
// when i delete the category, its children will be automatically deleted
CategorySchema.pre('findOneAndDelete', async function (next) {
    console.log(this.getQuery()._id);
   const categories = await this.model.find({parent:this.getQuery()._id});
    console.log(categories);
   await this.model.deleteMany({parent: this.getQuery()._id}).then(function () {
        console.log("categories son's are deleted"); // Success
    }).catch(function (error) {
        console.log(error); // Failure
    });
   next();
});
// Compile model from schema
var Category = mongoose.model('Category', CategorySchema );
module.exports = Category;
