var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var AuthRequestSchema = new Schema({
    email:  {
        type:String,
        required: [true, 'Username  obligatoire']
    },
    password:{
        type:String,
        minlength:8,
        required: [true, 'Password invalid']
    },
});
// Compile model from schema
var AuthRequest = mongoose.model('AuthRequest', AuthRequestSchema );
module.exports = AuthRequest;
