var mongoose = require('mongoose');
require('path').default;
const orderdetailModel = require('./OrderDetail');
//Define a schema
var Schema = mongoose.Schema;

var OrderSchema = new Schema({
    costumerId: {
        type: Schema.ObjectId,
        ref: 'User',
        required: [true, 'costumerId  obligatoire']
    },
    createdDate: {
        type: Date,
        min: Date.now(),
        default: Date.now(),
        required: [true, 'Date invalid']
    },
    Ordredproducts: [{type: Schema.ObjectId, ref: 'OrderDetail'}],
    status: {
        type: String,
        enum: ['New', 'Hold', 'Shipped', "Delivred", "Close"],
        required: [true, 'Status invalid']
    },
    total: {
        type: Number,
    }
});

//when the order is deleted, all the details of the order related to it will be deleted
OrderSchema.pre('findOneAndDelete',async function (next) {
    console.log("Delete Order");
    await orderdetailModel.deleteMany({orderId: this._conditions._id}).then(function () {
        console.log("Data deleted"); // Success
    }).catch(function (error) {
        console.log(error); // Failure
    });
    next();
});
//some things that i can do it after updating an order
OrderSchema.pre('findOneAndUpdate', async function (next) {
    //order that has been updated
    const UpdatedOrder = await this.model.findOne(this.getQuery()).populate('Ordredproducts');
    //Orderdetails that have been linked to updated order in orderdetails document
    const ordersDetails = await orderdetailModel.find({orderId:UpdatedOrder._id});

//this section for deleting orderdetails that linked to Order and do not exist into her list orderedproducts
    var first = JSON.parse(JSON.stringify(ordersDetails));
    var second = JSON.parse(JSON.stringify(UpdatedOrder.Ordredproducts));
    var difference = diff(first, second);
    difference.forEach(async e=>await orderdetailModel.deleteMany({_id:e._id}));
//this section for updating total price of order if there is a modification in ordred list
    let total = 0;
    UpdatedOrder.Ordredproducts.forEach(e=>{total=total+e.total});
    this._update.total=total;

    next();
});

function diff(a1, a2) {
    let diff = [];
    let arr1 = a1;
    let arr2 = a2;
    if (a1.length > a2.length) {
        console.log("a1>");

        for (let i = 0; i < arr1.length; i++) {
            let exist=false;
            for (let j = 0; j < arr2.length; j++) {
                if (arr1[i]._id === arr2[j]._id ) {
                   exist=true;
                }
            }
            if(exist===false)
                diff.push(arr1[i]);
        }

    } else {
        for (let i = 0; i < arr2.length; i++) {
            let exist=false;
            for (let j = 0; j < arr1.length; j++) {
                if (arr2[i]._id === arr1[j]._id) {
                    exist=true;
                }
            }
            if(exist===false)
                diff.push(arr2[i]);
        }
    }
    return diff;
}

// Compile model from schema
var Order = mongoose.model('Order', OrderSchema);


module.exports = Order;
