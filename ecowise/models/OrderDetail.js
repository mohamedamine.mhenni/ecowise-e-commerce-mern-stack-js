var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var OrderDetailSchema = new Schema({
    orderId:   {
        type: Schema.ObjectId, ref: 'Order'
    },
    productId:  {
        type: Schema.ObjectId, ref: 'Product'
    },
    quantity:{
        type:Number,
        min: 1,
        required:[true,'Quantité obligatoire']
    },
    total:{
        type:Number,
    }
});


// Compile model from schema
var OrderDetail = mongoose.model('OrderDetail', OrderDetailSchema );

module.exports = OrderDetail;
