//Require Mongoose
var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    firstname:  {
        type:String
    },
    lastname:  {
        type:String
    },
    username:  {
        type:String
    },
    password:{
        type:String,
        minlength:8,
        required: [true, 'Password invalid']
    },
    email:{
        type:String,
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address'],
        required:[true, 'Email  Oligatoire']
    },
    phone : {
        type:String
    },
    companyname : {
        type:String,
    },
    country: {
        type:String,
    },
    companydescription : {
        type:String,
    },
    city: {
        type:String,
    },
    adress:{
        type:String,
        required:[true, 'Adress Obligatoire']
    },
    role:{
        type:String,
        enum: ['admin', 'company', 'client']
    },
});
//When I update user do somethings
UserSchema.pre('findOneAndUpdate', function() {
    console.log("findAndUpdate");
});
//When I delete user do somethings
UserSchema.pre('findOneAndDelete', function() {
    console.log("findAndDelete");
});

// Compile model from schema
var User = mongoose.model('User', UserSchema );

module.exports = User;
