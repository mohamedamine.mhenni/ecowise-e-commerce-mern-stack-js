var express = require('express');
var env = require('dotenv').config();
var router = express.Router();
const multer = require('multer');
const productModel = require('../models/Product');
var authJwtHelper = require('../Helpers/AuthenticateJWT');
const fs = require('fs');
const storage = multer.diskStorage({
    destination: './uploads/products',
    filename: function (req, file, cb) {

        // Mimetype stores the file type, set extensions according to filetype
        switch (file.mimetype) {
            case 'image/jpeg':
                ext = '.jpeg';
                break;
            case 'image/png':
                ext = '.png';
                break;
            case 'image/gif':
                ext = '.gif';
                break;
        }

        cb(null, file.originalname + ext);
    }
});
const upload = multer({storage: storage});

router.get('/Allproducts', authJwtHelper,async (req, res) => {
    const products = await productModel.find({},{pictures:0}).populate('owner').populate('category');

    try {
        // console.log(products);
        res.status(200).send(products);
    } catch (err) {
        res.status(500).send(err);
    }
});
router.post('/addProduct', upload.array('files'), async (req, res) => {
    const product = new productModel(req.body);
    try {
        const InsertedProduct = await productModel.create({
            title: product.title,
            owner: product.owner,
            description: product.description,
            category: product.category,
            price: product.price,
            image:req.files[0].filename,
            pictures: req.files
        });
        res.send(InsertedProduct);
    } catch (err) {
        res.status(500).send(err);
    }
});
router.delete('/deleteProduct/:id', async (req, res) => {
    try {
        const product = await productModel.findByIdAndDelete(req.params.id);

        if (!product) res.status(404).send("No product found");
        res.status(200).send()
    } catch (err) {
        res.status(500).send(err)
    }
});
router.patch('/updateProduct/:id', async (req, res) => {
    console.log(req.body);
    try {
        const product = await productModel.findByIdAndUpdate(req.params.id, req.body);
        res.status(200).send(product);
    } catch (err) {
        res.status(500).send(err)
    }
});

router.get('/getProductByCategory/:id', async (req, res) => {
    try {
        const products = await productModel.find({category: req.params.id}).populate('category');
        res.status(200).send(products);
    } catch (err) {
        console.log(err);
    }
});
router.get('/getById/:id',async (req,res)=>{
    try{
        const product = await productModel.findById(req.params.id).populate('category').populate('owner');
        res.status(200).send(product)
    }catch (e) {
        res.status(500).send(e.message);
    }
});
//send images to front formated to base64
router.post('/getproductimg', async (req, res) => {
    var result = [];
    try {
        var imagesNames = req.body.imagenames;
        for (let i = 0; i < imagesNames.length; i++) {
          var img =fs.readFileSync('./uploads/products/' + imagesNames[i].filename);
          var base64 = Buffer.from(img).toString('base64');
          base64 = 'data:' + imagesNames[i].mimetype + ';base64,' + base64;
          result.push(base64);
        }
       res.status(200).send(result);
    } catch (err) {
        res.status(500).send(err);
    }
});

function f(image) {
    try {
        console.log(img.filename);
        var img = fs.readFile('./uploads/products/' + image.filename, function (err, data) {
            var base64 = Buffer.from(data).toString('base64');
            base64 = 'data:' + image.mimetype + ';base64,' + base64;
            return base64;
        });
    } catch (err) {
        console.log(err.message)
    }

}


module.exports = router;
