var express = require('express');
var env = require('dotenv').config();
var router = express.Router();
const userModel = require('../models/User');
var authJwtHelper = require('../Helpers/AuthenticateJWT');
const bcrypt = require('bcrypt');
const saltRounds = 10;



/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});
router.get('/AllUsers', async (req, res) => {
    const users = await userModel.find({});

    try {
        res.send(users);
    } catch (err) {
        res.status(500).send(err);
    }
});
router.post('/adduser', async (req, res) => {
    console.log(req.body);
    const user = new userModel(req.body);
    console.log(user);
    try {
        const hashedPwd = await bcrypt.hash(user.password, saltRounds);
        const InsertedUser = await userModel.create({
            firstname:user.firstname,
            lastname:user.lastname,
            username: user.username,
            password: hashedPwd,
            email:user.email,
            companyname:user.companyname,
            companydescription:user.companydescription,
            country:user.country,
            city:user.city,
            phone:user.phone,
            adress:user.adress,
            role:user.role
        });
        res.send(InsertedUser);
    } catch (err) {
        res.status(500).send(err);
    }
});
router.delete('/deleteUser/:id',async (req, res) => {
    try {
        const user = await userModel.findByIdAndDelete(req.params.id);

        if (!user) res.status(404).send("No item found");
        res.status(200).send()
    } catch (err) {
        res.status(500).send(err)
    }
});
router.patch('/updateUser/:id',async (req, res) => {
    try {
        const user = await userModel.findByIdAndUpdate(req.params.id, req.body);
        //await user.save();
        res.status(200).send(user);
    } catch (err) {
        res.status(500).send(err)
    }
});




module.exports = router;
