var express = require('express');
var env = require('dotenv').config();
var router = express.Router();
const orderModel = require('../models/Order');
const orderDetailsModel = require('../models/OrderDetail');
var authJwtHelper = require('../Helpers/AuthenticateJWT');

router.get('/findById/:id',async (req,res)=>{

  try{
    const order = await orderModel.findById(req.params.id).populate('Ordredproducts');
    res.status(200).send(order);
  }catch (err) {
    res.status(500).send(err);
  }
});
router.get('/Allorders', async (req, res) => {
  const orders = await orderModel.find({}).populate('Ordredproducts');

  try {
    res.status(200).send(orders);
  } catch (err) {
    res.status(500).send(err);
  }
});
router.post('/addOrder2', async (req, res) => {
  try {
    const order = new orderModel(req.body);
    const result = await order.save();
    res.send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});
router.post('/addOrder', async (req, res) => {
  var ordredproducts=[];
  const order = new orderModel(req.body);
  req.body.Ordredproducts.forEach(e=>{
    let od =new orderDetailsModel(e);
    od.orderId=order._id;
    ordredproducts.push(od);
  });

  try {

    const InsertedOrder = await orderModel.create({
      _id:order._id,
      costumerId: order.costumerId,
      Ordredproducts:!order.Ordredproducts?null:ordredproducts.map(e=>{return e._id}),
      status:order.status
    });
    for (const e of ordredproducts) {
     await  orderDetailsModel.create(e);
    }
    res.send(InsertedOrder);
  } catch (err) {
    console.log(err.message);
    res.status(500).send(err);
  }
});
router.delete('/deleteOrder/:id',async (req, res) => {
  try {
    const order = await orderModel.findByIdAndDelete(req.params.id);

    if (!order) res.status(404).send("No order found");
    res.status(200).send()
  } catch (err) {
    res.status(500).send(err)
  }
});

router.patch('/updateOrder/:id',async (req, res) => {
  try {
    const order = await orderModel.findByIdAndUpdate(req.params.id, req.body);
    res.send(order);
  } catch (err) {
    console.log(err);
    res.status(500).send(err)
  }
});




module.exports = router;
