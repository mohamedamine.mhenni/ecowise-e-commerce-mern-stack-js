var express = require('express');
var env = require('dotenv').config();
var router = express.Router();
const categoryModel = require('../models/Category');
var authJwtHelper = require('../Helpers/AuthenticateJWT');


router.get('/Allcategories', async (req, res) => {
  const categories = await categoryModel.find({}).populate('parent');

  try {
    res.status(200).send(categories);
  } catch (err) {
    res.status(500).send(err);
  }
});
router.post('/addCategory', async (req, res) => {
  const category = new categoryModel(req.body);
  try {
    const InsertedCategory = await categoryModel.create({
      title: category.title,
      description: category.description,
      parent:!category.parent?null:category.parent
    });
    res.send(InsertedCategory);
  } catch (err) {
    res.status(500).send(err);
  }
});
router.delete('/deleteCategory/:id',async (req, res) => {
  try {
    const category = await categoryModel.findByIdAndDelete(req.params.id);

    if (!category) res.status(404).send("No category found");
    res.status(200).send()
  } catch (err) {
    res.status(500).send(err)
  }
});
router.patch('/updateCategory/:id',async (req, res) => {
  try {
    const category = await categoryModel.findByIdAndUpdate(req.params.id, req.body);
    res.status(200).send(category);
  } catch (err) {
    res.status(500).send(err)
  }
});

router.get('/Subcategories/:id', async (req, res) => {
  const Subcategories = await categoryModel.find({parent:req.params.id}).populate('category');
  try {
    res.status(200).send(Subcategories);
  } catch (err) {
    res.status(500).send(err);
  }
});



module.exports = router;
