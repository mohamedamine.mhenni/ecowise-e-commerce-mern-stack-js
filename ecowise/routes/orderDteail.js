var express = require('express');
var env = require('dotenv').config();
var router = express.Router();
const orderDetailModel = require('../models/OrderDetail');
const productModel = require('../models/Product');
const orderModel = require('../models/Order');
var authJwtHelper = require('../Helpers/AuthenticateJWT');




router.get('/AllordersDetails', async (req, res) => {
  const ordersDetails = await orderDetailModel.find({}).populate('productId').populate('orderId');

  try {
    res.status(200).send(ordersDetails);
  } catch (err) {
    res.status(500).send(err);
  }
});
router.get('/AllordersDetails/:orderId', async (req, res) => {
  const ordersDetails = await orderDetailModel.find({orderId:req.params.orderId})
      .populate('productId').populate({
        path: 'productId',
        populate: { path: 'owner' }
      }).populate('orderId');

  try {
    res.status(200).send(ordersDetails);
  } catch (err) {
    res.status(500).send(err);
  }
});
router.post('/addOrderDetails', async (req, res) => {
  const orderdetail = new orderDetailModel(req.body);
  console.log(orderdetail);
  try {
    const product = await productModel.findById(orderdetail.productId);
    orderdetail.total=product.price*orderdetail.quantity;
    const InsertedOrderDetail = await orderDetailModel.create(orderdetail);
    await orderModel.findByIdAndUpdate(
        InsertedOrderDetail.orderId,
        { $push: { Ordredproducts: InsertedOrderDetail._id } },
        { new: true, useFindAndModify: false }
    );
    res.send(InsertedOrderDetail);
  } catch (err) {
    console.log(err);
    res.status(500).send(err);
  }
});
router.delete('/deleteOrderDetail/:id',async (req, res) => {
  try {
    const orderdetail = await orderDetailModel.findByIdAndDelete(req.params.id);

    if (!orderdetail) res.status(404).send("No orderdetail found");
    res.status(200).send()
  } catch (err) {
    res.status(500).send(err)
  }
});
router.put('/updateOrderDetail/:id',async (req, res)=>{
  try {
    const orderdetail = await orderDetailModel.findById(req.params.id).populate('productId');
    const total=req.body.quantity*orderdetail.productId.price;
    console.log(total);
    orderdetail.quantity=req.body.quantity;
    orderdetail.total=total;
    res.status(200).send(await orderDetailModel.findByIdAndUpdate(req.params.id,orderdetail) );
  } catch (err) {
    res.status(500).send(err)
  }
});/* => {
  try {

    const orderdetail =  await orderDetailModel.findById(req.params.id).populate('productId');
    orderdetail.total=orderdetail.quantity*orderdetail.productId.price;
    const orderDetails = await orderDetailModel.findByIdAndUpdate(req.params.id,orderdetail);
    res.status(200).send(orderDetails);
  } catch (err) {
    res.status(500).send(err)
  }
});*/


module.exports = router;
