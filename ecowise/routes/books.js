/*
var express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken');
const accessTokenSecret = process.env.ACCESS_TOKEN_SECRET;
const books = [
  {
    "author": "Chinua Achebe",
    "country": "Nigeria",
    "language": "English",
    "pages": 209,
    "title": "Things Fall Apart",
    "year": 1958
  },
  {
    "author": "Hans Christian Andersen",
    "country": "Denmark",
    "language": "Danish",
    "pages": 784,
    "title": "Fairy tales",
    "year": 1836
  },
  {
    "author": "Dante Alighieri",
    "country": "Italy",
    "language": "Italian",
    "pages": 928,
    "title": "The Divine Comedy",
    "year": 1315
  },
];
/!*const authenticateJWT = (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (authHeader) {
    const token = authHeader.split(' ')[1];

    jwt.verify(token, accessTokenSecret, (err, user) => {
      if (err) {
        return res.sendStatus(403);
      }

      req.user = user;
      next();
    });
  } else {
    res.sendStatus(401);
  }
};*!/
/!* GET users listing. *!/
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
router.get('/Allbooks', authenticateJWT, (req, res) => {
  const { role } = req.user;
  if (role !== 'admin') {

    return res.send("your are not a admin");
  }
  res.json(books);
});
router.post('/addbook', authenticateJWT, (req, res) => {
  const { role } = req.user;

  if (role !== 'admin') {

    return res.sendStatus(403);
  }


  const book = req.body;
  books.push(book);

  res.send('Book added successfully');
});
module.exports = router;
*/
