var express = require('express');
var env = require('dotenv').config();
var router = express.Router();
const userModel = require('../models/User');
const authrequestModel = require('../models/AuthenticationRequest');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');
const accessTokenSecret = process.env.ACCESS_TOKEN_SECRET;
var refreshTokens = [];
const refreshTokenSecret = process.env.REFRESH_TOKEN_SECRET;
var accestokentest = "";


//Generate token one time
/*router.post('/login', function(req, res, next){
// Read username and password from request body
  const { username, password } = req.body;
  console.log(req.body);
  // Filter user from the users array by username and password
  const user = users.find(u => { return u.username === username && u.password === password });

  if (user) {
    // Generate an access token
    const accessToken = jwt.sign({ username: user.username,  role: user.role }, accessTokenSecret);

    res.json({
      accessToken
    });
  } else {
    res.send('Username or password incorrect');
  }
});*/

//Generate token & refresh token
router.post('/login', async(req, res) => {
    // read username and password from request body
    const authrequest = new authrequestModel(req.body);
    // filter user from the users array by username
    const user =await  userModel.findOne({'email': authrequest.email});

    if (user) {

        const cmp = await bcrypt.compare(authrequest.password, user.password);
        if (cmp) {
            // generate an access token
            const accessToken = jwt.sign({username: user.email, role: user.role}, accessTokenSecret, {expiresIn: '1m'});
            const refreshToken = jwt.sign({username: user.email, role: user.role}, refreshTokenSecret);
            accestokentest = accessToken;
            console.log(accessToken);

            refreshTokens.push(refreshToken);
            console.log(`refreshToken []:${refreshTokens}`);
            res.json({
                accessToken,
                refreshToken
            });
        }else{
            res.status(404).send('Incorrect password');
        }

    } else {
        res.status(404).send('Username not found');
    }
});
//Regenrate token à l'aide de refreshToken
router.post('/token', (req, res) => {
    const {token} = req.body;
    console.log("************Refresh token method****************");
    console.log(req.body);
    console.log(token);
    if (!token) {
        return res.sendStatus(401);
    }

    if (!refreshTokens.includes(token)) {
        console.log("--------------------------------");
        console.log(token);
        console.log("refresh token does not exist in refreshTokens[]");
        return res.sendStatus(403);
    }

    jwt.verify(token, refreshTokenSecret, (err, user) => {
        if (err) {
            console.log("refresh token is not verified");
            return res.sendStatus(401);
        }

        const accessToken = jwt.sign({username: user.username, role: user.role}, accessTokenSecret, {expiresIn: '1m'});
        console.log(`the old access token is : ${accestokentest}`);
        console.log(`the new access token is : ${accessToken}`);
        res.json({
            accessToken
        });
    });
});
router.post('/registration', async (req, res) => {
    const user = new userModel(req.body);
    try {
        const hashedPwd = await bcrypt.hash(user.password, saltRounds);
        const InsertedUser = await userModel.create({
            firstname:user.firstname,
            lastname:user.lastname,
            username: user.username,
            password: hashedPwd,
            email:user.email,
            companyname:user.companyname,
            companydescription:user.companydescription,
            country:user.country,
            city:user.city,
            phone:user.phone,
            adress:user.adress,
            role:user.role
        });
        res.send(InsertedUser);
    } catch (err) {
        res.status(500).send(err);
    }
});
router.post('/logout', (req, res) => {
    const {token} = req.body;
    console.log(`token log out ${token}`);
    refreshTokens = refreshTokens.filter(token => t !== token);
    res.send("Logout successful");
});
module.exports = router;
