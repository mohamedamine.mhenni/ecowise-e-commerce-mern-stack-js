import { combineReducers } from 'redux';
import auth from "./auth/reducer";
import message from "./message/reducer";
import user from "./user/reducer";
import product from "./product/reducer";
import category from "./category/reducer";
import order from "./Order/reducer";
export default combineReducers({
    auth,
    message,
    user,
    product,
    category,
    order
});
