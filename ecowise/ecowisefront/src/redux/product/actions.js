import {
    GET_PRODUCTS_SUCCESS,
    GET_PRODUCTS_FAIL,
    ADD_PRODUCTS_SUCCESS,
    ADD_PRODUCTS_FAIL,
    DELETE_PRODUCTS_SUCCESS,
    DELETE_PRODUCTS_FAIL,
    SELECT_PRODUCTS_SUCCESS, UPDATE_PRODUCTS_SUCCESS, UPDATE_PRODUCTS_FAIL, PRODUCTS_GET_LIST_WITH_FILTER
} from "../actions";
import ProductService from "../../Services/Product/ProductService.js";
export const getallproducts = () => (dispatch) => {
return ProductService.getAllproducts().then(
    (response) => {
        dispatch({
            type: GET_PRODUCTS_SUCCESS,
            payload:response
        });
        console.log(response);
        return Promise.resolve();
    },
    (error) => {
        const message =
            (error.response &&
                error.response.data &&
                error.response.data.message) ||
            error.message ||
            error.toString();

        dispatch({
            type: GET_PRODUCTS_FAIL,
            payload:message
        });



        return Promise.reject();
    }
);
};

export const addnewproduct = (title,owner,description,category,price,pictures) => (dispatch) => {
    return ProductService.addNewProduct(title,owner,description,category,price,pictures).then(
        (response) => {
            dispatch({
                type: ADD_PRODUCTS_SUCCESS,
                payload:response
            });
            return Promise.resolve();
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            dispatch({
                type: ADD_PRODUCTS_FAIL,
                payload:message
            });



            return Promise.reject();
        }
    );
};

export const updateproduct = (id,title,owner,description,category,price) => (dispatch) => {
    return ProductService.updateProduct(id,title,owner,description,category,price).then(
        (response) => {
            dispatch({
                type: UPDATE_PRODUCTS_SUCCESS,
                payload:response
            });
            return Promise.resolve();
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            dispatch({
                type: UPDATE_PRODUCTS_FAIL,
                payload:message
            });



            return Promise.reject();
        }
    );
};

export const deleteproduct = (productId) => (dispatch) => {
    return ProductService.deleteProduct(productId).then(
        (response) => {
            dispatch({
                type: DELETE_PRODUCTS_SUCCESS,
                payload:productId
            });
            return Promise.resolve();
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            dispatch({
                type: DELETE_PRODUCTS_FAIL,
                payload:message
            });



            return Promise.reject();
        }
    );
};

export const  selectproduct=(product)=>(dispatch)=>{

        {dispatch({
            type: SELECT_PRODUCTS_SUCCESS,
            payload:product
        });
        return Promise.resolve();}

};
export const productfilter=(filter)=>(dispatch)=>{
    {
        dispatch({
            type:PRODUCTS_GET_LIST_WITH_FILTER,
            payload:filter
        });
        return Promise.resolve();
    }
};

