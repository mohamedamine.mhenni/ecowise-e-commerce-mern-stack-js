import {
    GET_PRODUCTS_SUCCESS,
    GET_PRODUCTS_FAIL,
    ADD_PRODUCTS_SUCCESS,
    ADD_PRODUCTS_FAIL,
    DELETE_PRODUCTS_SUCCESS,
    DELETE_PRODUCTS_FAIL,
    SELECT_PRODUCTS_SUCCESS, PRODUCTS_GET_LIST_WITH_FILTER
} from "../actions";


const initialState = {
    isLoading: true,
    productsList: [],
    productsItems: [],
    selectedProducts: [],
    error: ""
};

export default function (state = initialState, action) {
    const {type, payload} = action;

    switch (type) {
        case GET_PRODUCTS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                productsList: payload,
                productsItems: payload
            };
        case GET_PRODUCTS_FAIL:
            return {
                ...state,
                isLoading: true,
                error: payload
            };
        case ADD_PRODUCTS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                productsList: [...state.productsList, payload],
                productsItems: [...state.productsItems, payload]
            };
        case ADD_PRODUCTS_FAIL:
            return {
                ...state,
                isLoading: true,
                error: payload
            };
        case DELETE_PRODUCTS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                productsList: state.productsList.filter(p => p._id !== payload)
            };
        case DELETE_PRODUCTS_FAIL:
            return {
                ...state,
                isLoading: true,
                error: payload
            };
        case   SELECT_PRODUCTS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                selectedProducts: [...state.selectedProducts, payload]
            };
        case PRODUCTS_GET_LIST_WITH_FILTER:
            console.log(payload);
            state.productsItems = state.productsList;
            let filtred;
            if (payload) {
                 filtred =
                    payload.category ? state.productsItems = state.productsItems.filter(p => p.category.title === payload.category) : state.productsItems;
                filtred = payload.pricerange ? state.productsItems = state.productsItems.filter(p => p.price >= payload.pricerange[0] &&
                    p.price <= payload.pricerange[1]) : state.productsItems;
                filtred = payload.title ? state.productsItems = state.productsItems.filter(p => p.title.toLowerCase().includes(payload.title.toLowerCase())) : state.productsItems
            }
            console.log(filtred);
            return {
                ...state,
                isLoading: false,
                productsItems: filtred
            };

        default:
            return state;
    }
}
