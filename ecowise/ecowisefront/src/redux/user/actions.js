import {
    ADD_USER_FAIL,
    ADD_USER_SUCCESS, DELETE_USER_FAIL,
    DELETE_USER_SUCCESS,
    GET_USERS_FAIL,
    GET_USERS_SUCCESS,
    UPDATE_USER_FAIL,
    UPDATE_USER_SUCCESS,
    SELECT_USER_SUCCESS,
    SET_MESSAGE, USER_GET_LIST_WITH_FILTER
} from "../actions";
import UserService from "../../Services/User/UserService.js";

export const getallusers = (orderBy, search) => (dispatch) => {
    return UserService.getAllUsers(orderBy, search).then(
        (data) => {
            dispatch({
                type: GET_USERS_SUCCESS,
                payload: {users: data}
            });

            return Promise.resolve();
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            dispatch({
                type: GET_USERS_FAIL,
            });

            dispatch({
                type: SET_MESSAGE,
                payload: message,
            });

            return Promise.reject();
        }
    );
};
export const addnewuser = (firstname, lastname, username, password, email, adress, role) => (dispatch) => {
    return UserService.addNewUser(firstname, lastname, username, password, email, adress, role).then(
        (data) => {
            dispatch({
                type: ADD_USER_SUCCESS,
                payload: data
            });
            return Promise.resolve();
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            dispatch({
                type: ADD_USER_FAIL
            });
            dispatch({
                type: SET_MESSAGE,
                payload: {message: message}
            })
        });
};
export const deleteuser = (userId) => (dispatch) => {
    return UserService.deleteUser(userId).then(
        (data) => {
            dispatch({
                type: DELETE_USER_SUCCESS,
                payload: userId
            });

            return Promise.resolve();
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            dispatch({
                type: DELETE_USER_FAIL,
            });

            dispatch({
                type: SET_MESSAGE,
                payload: message,
            });

            return Promise.reject();
        }
    );
};
export const updateuser = (userId, firstname, lastname, username, password, email, adress, role) => (dispatch) => {
    return UserService.updateUser(userId, firstname, lastname, username, password, email, adress, role).then(
        (data) => {
            dispatch({
                type: UPDATE_USER_SUCCESS
            });

            return Promise.resolve();
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            dispatch({
                type: UPDATE_USER_FAIL,
            });

            dispatch({
                type: SET_MESSAGE,
                payload: message,
            });

            return Promise.reject();
        }
    );
};
export const selectusers = (user) => (dispatch) => {


    {
        dispatch({
            type: SELECT_USER_SUCCESS,
            payload: user
        });

        return Promise.resolve();
    }

};
export const userfilter=(filter)=>(dispatch)=>{
    {
        dispatch({
            type:USER_GET_LIST_WITH_FILTER,
            payload:filter
        });
        return Promise.resolve();
    }
};

