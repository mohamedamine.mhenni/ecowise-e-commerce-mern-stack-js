import {
    GET_USERS_FAIL,
    GET_USERS_SUCCESS,
    ADD_USER_FAIL,
    ADD_USER_SUCCESS,
    DELETE_USER_FAIL,
    DELETE_USER_SUCCESS,
    UPDATE_USER_FAIL,
    UPDATE_USER_SUCCESS,
    SELECT_USER_SUCCESS,
    PRODUCTS_GET_LIST_WITH_FILTER,
    USER_GET_LIST_WITH_FILTER
} from "../actions";



const initialState = { isLoading: true, users:[],selectedUsers:[],usersItems: [], };

export default function (state = initialState, action) {
    const { type, payload } = action;

    switch (type) {
        case GET_USERS_SUCCESS:

            return {
                ...state,
                isLoading: false,
                users: payload.users,
                usersItems:payload.users
            };
        case GET_USERS_FAIL:
            return {
                ...state,
                isLoading: true,
            };
        case ADD_USER_SUCCESS:
            return {
                ...state,
                isLoading: false,
                users:[...state.users,payload]
            };
        case ADD_USER_FAIL:
            return {
                ...state,
                isLoading: true,
            };
        case DELETE_USER_SUCCESS:
            return {
                ...state,
                isLoading: false,
                users:state.users.filter((item) =>
                    item._id!== payload)
            };
        case DELETE_USER_FAIL:
            return {
                ...state,
                isLoading: true,
            };
        case UPDATE_USER_SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case UPDATE_USER_FAIL:
            return {
                ...state,
                isLoading: true,
            };
        case SELECT_USER_SUCCESS:
            return {
                ...state,
                isLoading: false,
                selectedUsers: [...state.selectedUsers,payload]
            };
        case USER_GET_LIST_WITH_FILTER:
            state.usersItems = state.users;
            let filtred;
            console.log(payload);
            if (payload) {
                filtred =payload.role ? state.usersItems = state.usersItems.filter(u => u.role === payload.role) : state.usersItems;
                filtred = payload.search ? state.usersItems = state.usersItems.filter(u => u.firstname.toLowerCase().includes(payload.search.toLowerCase())
                    || u.lastname.includes(payload.search) || u.email.includes(payload.search)
                ) : state.usersItems;

            }
            return {
                ...state,
                isLoading: false,
                usersItems: filtred
            };
        default:
            return state;
    }
}
