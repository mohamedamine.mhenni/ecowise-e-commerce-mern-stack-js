import {
    GET_CATEGORYS_FAIL,
    GET_CATEGORYS_SUCCESS,
    DELETE_CATEGORYS_FAIL,
    DELETE_CATEGORYS_SUCCESS,
    ADD_CATEGORYS_FAIL,
    ADD_CATEGORYS_SUCCESS,
    UPDATE_CATEGORYS_FAIL, UPDATE_CATEGORYS_SUCCESS, SELECT_CATEGORYS_SUCCESS
} from "../actions";
import CategoryService from "../../Services/Category/CategoryService";

export const getallcategories = () => (dispatch) => {
    return CategoryService.getAllcategories().then((data) => {
            dispatch({
                type: GET_CATEGORYS_SUCCESS,
                payload: data
            });
            return Promise.resolve();
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();
            dispatch({
                type: GET_CATEGORYS_FAIL,
                payload: message
            });
            return Promise.reject();
        }
    )

};
export const addnewcategory = (title, description, parent) => (dispatch) => {
    return CategoryService.addNewCategory(title, description, parent).then((data) => {
            dispatch({
                type: ADD_CATEGORYS_SUCCESS,
                payload: data
            });
            return Promise.resolve();
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();
            dispatch({
                type: ADD_CATEGORYS_FAIL,
                payload: message
            });
            return Promise.reject();
        }
    )

};
export const deletecategory = (categoryId) => (dispatch) => {
    return CategoryService.deleteCategory(categoryId).then((data) => {
            dispatch({
                type: DELETE_CATEGORYS_SUCCESS,
                payload: categoryId
            });
            return Promise.resolve();
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();
            dispatch({
                type: DELETE_CATEGORYS_FAIL,
                payload: message
            });
            return Promise.reject();
        }
    )

};
export const updatecategory = (id, title, description, parent) => (dispatch) => {
    return CategoryService.updateCategory(id, title, description, parent).then((data) => {
            dispatch({
                type: UPDATE_CATEGORYS_SUCCESS,
                payload: data
            });
            return Promise.resolve();
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();
            dispatch({
                type: UPDATE_CATEGORYS_FAIL,
                payload: message
            });
            return Promise.reject();
        }
    )

};
export const selectcategories = (category) => (dispatch) => {
    {
        dispatch({
            type: SELECT_CATEGORYS_SUCCESS,
            payload: category
        });
        return Promise.resolve();
    }
};
