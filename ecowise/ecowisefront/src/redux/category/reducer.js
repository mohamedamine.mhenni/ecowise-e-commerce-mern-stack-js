import {
    GET_CATEGORYS_FAIL,
    GET_CATEGORYS_SUCCESS,
    DELETE_CATEGORYS_FAIL,
    DELETE_CATEGORYS_SUCCESS,
    ADD_CATEGORYS_FAIL,
    ADD_CATEGORYS_SUCCESS,
    UPDATE_CATEGORYS_FAIL, UPDATE_CATEGORYS_SUCCESS, SELECT_CATEGORYS_SUCCESS
} from "../actions";
const initialState = { isLoading: true, categories:[],categoriesItems:[{_id:'',title:'No Parent'}],selectedCategories:[],error:'' };
export default function (state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
        case GET_CATEGORYS_SUCCESS:
        return {
            ...state,
            isLoading: false,
            categories: payload,
            categoriesItems: state.categoriesItems.concat(payload)
        };
        case GET_CATEGORYS_FAIL:
            return {
                ...state,
                isLoading: true,
                error: payload
            };
        case ADD_CATEGORYS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                categories: [...state.categories,payload],
                categoriesItems: [...state.categoriesItems,payload]
            };
        case ADD_CATEGORYS_FAIL:
            return {
                ...state,
                isLoading: true,
                error: payload
            };
        case DELETE_CATEGORYS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                categories: state.categories.filter(c=>c._id!==payload),
                categoriesItems: state.categoriesItems.filter(c=>c._id!==payload)
            };
        case DELETE_CATEGORYS_FAIL:
            return {
                ...state,
                isLoading: true,
                error: payload
            };
        case UPDATE_CATEGORYS_SUCCESS:
            return {
                ...state,
                isLoading: false
            };
        case UPDATE_CATEGORYS_FAIL:
            return {
                ...state,
                isLoading: true,
                error: payload
            };
        case SELECT_CATEGORYS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                selectedCategories: [...state.selectedCategories,payload]
            };
        default:
            return state;

    }
}
