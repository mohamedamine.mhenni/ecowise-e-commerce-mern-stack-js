import {
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOG_OUT,
} from "../actions";
const user = JSON.parse(localStorage.getItem("user"));

const initialState = user
    ? {
        isLoggedIn: true,
        user,
        error:null
}
    : {
        isLoggedIn: false,
        user: null,
        registred:false,
        error:null
};

export default function (state = initialState, action) {
    const { type, payload } = action;

    switch (type) {
        case REGISTER_SUCCESS:
            return {
                ...state,
                isLoggedIn: false,
                registred:true
            };
        case REGISTER_FAIL:
            return {
                ...state,
                isLoggedIn: false,
                error:payload
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                isLoggedIn: true,
                user: payload.user,
            };
        case LOGIN_FAIL:
            return {
                ...state,
                isLoggedIn: false,
                user: null,
                error:payload
            };
        case LOG_OUT:
            return {
                ...state,
                isLoggedIn: false,
                user: null,
            };
        default:
            return state;
    }
}
