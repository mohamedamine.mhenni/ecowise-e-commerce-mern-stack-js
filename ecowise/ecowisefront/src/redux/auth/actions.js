import {
    REGISTER_FAIL,
    REGISTER_SUCCESS,
    LOGIN_FAIL,
    LOGIN_SUCCESS,
    LOG_OUT,SET_MESSAGE
} from "../actions";
import AuthService from "../../Services/Auth/AuthService.js";
export const register = (firstname,lastname,username,country,city,companyname,companydescription,phone,password,email,adress,role) => (dispatch) => {
return AuthService.registration(firstname,lastname,username,country,city,companyname,companydescription,phone,password,email,adress,role).then(
    (response) => {
        dispatch({
            type: REGISTER_SUCCESS,
        });

        return Promise.resolve();
    },
    (error) => {
        const message =
            (error.response &&
                error.response.data &&
                error.response.data.message) ||
            error.message ||
            error.toString();

        dispatch({
            type: REGISTER_FAIL,
            payload:message
        });


        return Promise.reject();
    }
);
};

export const login = (username, password) => (dispatch) => {
    return AuthService.login(username, password).then(
        (data) => {
            dispatch({
                type: LOGIN_SUCCESS,
                payload: { user: data },
            });

            return Promise.resolve();
        },
        (error) => {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            dispatch({
                type: LOGIN_FAIL,
                payload: message
            });

            return Promise.reject();
        }
    );
};

export const logout = () => (dispatch) => {
    AuthService.logout().then(()=>
        dispatch({
            type: LOG_OUT,
        })
    );

};
