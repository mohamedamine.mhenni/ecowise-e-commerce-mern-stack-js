import {GET_ORDER_SUCCESS,
GET_ORDER_FAIL,
ADD_ORDER_SUCCESS,
ADD_ORDER_FAIL,
UPDATE_ORDER_SUCCESS,
UPDATE_ORDER_FAIL,
DELETE_ORDER_SUCCESS,
DELETE_ORDER_FAIL,
SELECT_ORDER_SUCCESS}from '../actions';
import OrderService from "../../Services/Order/OrderService";
export const getallorders=()=>(dispatch)=>{
 return OrderService.getAllOrder().then(
     (data)=>{
         dispatch({
             type:GET_ORDER_SUCCESS,
             payload:data
         });
         return Promise.resolve();
     },
     (error)=>{
         const message=(error.response &&
             error.response.data &&
             error.response.data.message) ||
             error.message ||
             error.toString();
         dispatch({
             type:GET_ORDER_FAIL,
             payload: message
         });
         return Promise.reject();
     }
 )
};
export const addorder=(owner,ordredproducts,status,total)=>(dispatch)=>{
    console.log("add order from Order/action");
    return OrderService.addNewOrder(owner,ordredproducts,status,total).then(
        (data)=>{
            dispatch({
                type:ADD_ORDER_SUCCESS,
                payload:data
            });
            return Promise.resolve();
        },
        (error)=>{
            const message = (error.response &&
                error.response.data &&
                error.response.data.message) ||
                error.message ||
                error.toString();
            dispatch({
               type:ADD_ORDER_FAIL,
               payload:error
            });
            return Promise.reject();
        }
    )

};
export const deleteorder=(orderId)=>(dispatch)=>{
    return OrderService.deleteOrder(orderId).then(
        (data)=>{
            dispatch({
                type:DELETE_ORDER_SUCCESS,
                payload:orderId
            });
            return Promise.resolve();
        },
        (error)=>{
            const message = (error.response &&
                error.response.data &&
                error.response.data.message) ||
                error.message ||
                error.toString();
            dispatch({
                type:DELETE_ORDER_FAIL,
                payload:message
            });
            return Promise.reject();
        }
    )
};
export const updateorder=(id,owner,ordredproducts,status,total)=>(dispatch)=>{
        return OrderService.updateOrder(id,owner,ordredproducts,status,total).then(
            (data)=>{
                dispatch({
                    type:UPDATE_ORDER_SUCCESS,
                    payload:data
                });
                return Promise.resolve();
            },
            (error)=>{
                const message = (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                    error.message ||
                    error.toString();
                dispatch({
                    type:UPDATE_ORDER_FAIL,
                    payload:message
                });
                return Promise.reject();
            }
        )
};
export const selectorder=(orderdetails)=>(dispatch)=>{
    {
        dispatch({
            type:SELECT_ORDER_SUCCESS,
            payload:orderdetails
        });
        return Promise.resolve();
    }
};
