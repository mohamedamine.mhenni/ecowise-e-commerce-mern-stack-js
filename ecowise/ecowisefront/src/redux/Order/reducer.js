import {GET_ORDER_SUCCESS,
    GET_ORDER_FAIL,
    ADD_ORDER_SUCCESS,
    ADD_ORDER_FAIL,
    UPDATE_ORDER_SUCCESS,
    UPDATE_ORDER_FAIL,
    DELETE_ORDER_SUCCESS,
    DELETE_ORDER_FAIL,
    SELECT_ORDER_SUCCESS}from '../actions';

const initialState = {
    isLoading: true,
    orderList:[],
    selectedOrder:[],
    error:""
};

export default function(state=initialState,action) {
    const {type,payload} = action;
    switch (type) {
        case GET_ORDER_SUCCESS :
            return {
                ...state,
                isLoading: false,
                orderList: payload
            };
        case GET_ORDER_FAIL:
            return {
                ...state,
                isLoading: true,
                error: payload
            };
        case ADD_ORDER_SUCCESS:
            console.log("add order from Order/reducer");
            return {
                ...state,
                isLoading: false,
                orderList: [...state.orderList,payload]
            };
        case ADD_ORDER_FAIL:
            return{
                ...state,
                isLoading: true,
                error: payload
            };
        case DELETE_ORDER_SUCCESS:
            return {
                ...state,
                isLoading: false,
                orderList: state.orderList.filter(o=>o._id!==payload)
            };
        case DELETE_ORDER_FAIL :
            return {
                ...state,
                isLoading: true,
                error: payload
            };
        case UPDATE_ORDER_SUCCESS:
            return {
                ...state,
                isLoading: false
            };
        case UPDATE_ORDER_FAIL:
            return {
                ...state,
                isLoading: true,
                error: payload
            };
        case SELECT_ORDER_SUCCESS:
            return {
                ...state,
                isLoading: false,
                selectedOrder: [...state.selectedOrder,payload]
            };
        default:return state;
    }
}
