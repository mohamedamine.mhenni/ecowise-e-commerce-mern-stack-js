import React, {Component} from 'react';
import './helpers/axiosInterceptor';
import "bootstrap/dist/css/bootstrap.min.css";
import '@fortawesome/fontawesome-free';
import { connect } from "react-redux";
import {BrowserRouter as Router, Switch, Route, Link, Redirect} from "react-router-dom";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "react-datepicker/dist/react-datepicker.css";
import './App.css';

import AppRoutes from "./components/app/AppRoutes";
import UserRoutes from "./components/authentification/UserRoutes";
import Main from "./components/index";
import { logout } from "./redux/auth/actions";

import AdminRoutes from "./components/Admin/AdminRoutes";



const AuthRoute = ({ component: Component, auth, ...rest }) => (
<div>
    {console.log(auth)}
    <Route
        {...rest}

        render={props =>

            auth ? (

                <Component {...props} />
            ) : (
                <Redirect
                    to={{
                        pathname: "/authentication/login",
                    }}
                />
            )
        }
    />
</div>
);



const App= (props)=>  {



        const { isLoggedIn, user,match } =props;
console.log(props);

    return (
                    <Router >

                    <Switch>
                        <AuthRoute  path="/app" auth={isLoggedIn} component={AppRoutes} />
                        <Route  path="/authentication" component={UserRoutes} />
                        <Route  path="/admin" component={AdminRoutes} />
                        <Route path="/" exact component={Main} />
                    </Switch>
                    </Router>



    );

};

function mapStateToProps(state) {
    const { user,isLoggedIn } = state.auth;
    console.log(isLoggedIn);
    return {
        user,isLoggedIn
    };
}

export default connect(mapStateToProps)(App);
