import axios from 'axios';

const apiUrl = "/authentication";

axios.interceptors.request.use(
    config => {
        console.log("hello Im request interceptor");
        const token = localStorage.getItem('token');
        console.log(`Im the token thay get it from local storage  ${token}`);
        config.headers.authorization = `Bearer ${token}`;

        return config;
    },
    error => {
        return Promise.reject(error);
    },
);

axios.interceptors.response.use((response) => {
    return response
}, async function (error) {
    const originalRequest = error.config;
    if (error.response.status === 403 && !originalRequest._retry) {
        originalRequest._retry = true;
        const access_token = await getRefreshJwt();
        console.log(access_token.accessToken);
        // 1) put token to LocalStorage
        localStorage.setItem('token', access_token.accessToken);
        // 2) Change Authorization header
        axios.defaults.headers.common['authorization'] = 'Bearer ' + access_token.accessToken;
        // 3) return originalRequest object with Axios.
        return axios(originalRequest);
    }
    return Promise.reject(error);
});
const getRefreshJwt =  () => {
    return axios.post(`${apiUrl}/token`, {token: localStorage.getItem('refresh_token')})
        .then(res => {return res.data})
};
