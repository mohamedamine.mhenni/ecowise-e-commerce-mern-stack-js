import React, {useEffect, useState} from 'react';
import {Badge, Button} from "reactstrap";
import ProductService from "../../Services/Product/ProductService";
import DatePicker from "react-datepicker";

const OrderDetailsListView = (props) => {
    const {ordredproduct,status}=props;
    const [product,setproduct]=useState(null);
    const  getProduct= async (idproduct) => {
        return await ProductService.getproductById(idproduct).then((response) => {
            setproduct(response);
            return response;
        });
    };
    useEffect(()=>{
        getProduct(ordredproduct.productId);
    },[]);
    return product? (

            <div className="row" key={ordredproduct._id}>

                <div className="col-lg-2">
                    <p className="small text-black-50">{product.owner.username} </p>
                </div>
                <div className="col-lg-10">
                    <div className="row  ">

                        <div className="col-lg-2 ">
                            <img
                                alt={"image1"}
                                src={`http://localhost:3001/${product.image}`}
                                className="w-75 responsive border-6"
                            />
                        </div>
                        <div className="col">
                            <div className="row mb-2">
                                <div className="col-6">
                                    <div className="w-40 w-sm-100">
                                        <p className="list-item-heading mb-1 truncate">
                                            { product.title}
                                        </p>
                                    </div>
                                </div>
                                <div className="col-2">
                                    <div className="w-15 w-sm-100">
                                        <Badge color={'primary'} pill>
                                            {`${status}`}
                                        </Badge>
                                    </div>
                                </div>
                                <div className="col ">
                                    <div className="w-15 w-sm-100 float-right">
                                        <Button>Cancel Order</Button>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-5 ">
                                    <p className="mb-1  pt-2  text-black-50">
                                        {`${product.price} DT/Piece * ${ordredproduct.quantity}`}
                                    </p>
                                </div>
                                <div className="col-lg-7 ">
                                    <p className="mb-1  pt-2 float-right  text-black-50">
                                        {`Total=${ordredproduct.total} DT`}
                                    </p>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

    ):<div className="loading"/> ;
};

export default OrderDetailsListView;
