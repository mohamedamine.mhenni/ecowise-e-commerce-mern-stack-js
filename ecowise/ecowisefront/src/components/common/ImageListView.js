import React ,{useState,useEffect}from "react";
import {
    Row,
    Card,
    Button,
    CardBody,
    CardSubtitle,
    CardImg,
    CardText,
    CustomInput,
    Badge
} from "reactstrap";
import {connect} from "react-redux";
import { NavLink } from "react-router-dom";
import {selectproduct} from "../../redux/product/actions";
import {history} from "../../helpers/history";



const ImageListView = (props) => {
    const {product,selectproduct}=props;
    const [pictures,setpictures]=useState([]);
    console.log(props);
    useEffect( ()=>{
        console.log(props);
        //get Image converted to base64
       /*axios.post('/products/getproductimg',{imagenames:product.pictures}).then((response)=>{
           setpictures(response.data);
           return response.data;
        });*/

    },[]);
    return (

       <div className="mb-3 col-lg-4 col-sm-6 col-xl-3" key={product.id}>

                <Card>
                    <div className="position-relative" >
                        <NavLink to={`/app/productdetails/${product._id}`} className="w-40  w-sm-100">
                            <CardImg  top alt={product.title} src={product.image?`/${product.image}`:`/f9989d9e-8da0-40d8-a7b0-305df6cd7760.jpeg`} />
                        </NavLink>
                        <Badge
                            color={'primary'}
                            pill
                            className="position-absolute badge-top-left"
                        >
                            {`${product.price} DT`}
                        </Badge>
                    </div>
                    <CardBody>
                        <Row>
                            <div className="col-xs" >
                                <CustomInput
                                    className="item-check mb-0"
                                    type="checkbox"
                                    id={`check_${product._id}`}
                                    label=""
                                />
                            </div>
                            <div className="col"  >
                                <CardSubtitle><h6>{product.title}</h6></CardSubtitle>
                                <CardText className="text-muted text-small mb-0 font-weight-light">
                                    {product.description}
                                </CardText>
                            </div>

                        </Row>
                    </CardBody>
                </Card>

        </div>
    );
};


export default connect(null,{selectproduct}) (ImageListView);
