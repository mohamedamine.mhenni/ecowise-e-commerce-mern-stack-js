import React from 'react';

import OrderDetailsListView from "./OrderDetailsListView";
import DatePicker from "react-datepicker";
import {Link} from "react-router-dom";


const OrderListView = (props) => {
    const {order} = props;

    return (
        <div key={order._id} className=" mt-3">
            <div className="card">
                <div className="card-body">

                    <div className="d-flex  align-items-center justify-content-between">
                        <a className=" small text-black-50 ">Order N° : {order._id} | {order.createdDate}</a>
                        <Link to={"/app/orderdetails/"+order._id }className=" small text-black-50 "> show more </Link>
                    </div>
                    <hr/>
                    {order.Ordredproducts.map((op, i) => {
                        return (

                            <OrderDetailsListView
                                key={i}
                                ordredproduct={op}
                                status={order.status}
                            />
                        );
                    })}

                </div>


            </div>
        </div>
    );
};

export default OrderListView;
