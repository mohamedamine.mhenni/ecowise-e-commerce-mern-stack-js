import React, {useState} from "react";
import {Badge, Input} from "reactstrap";

const ThumbListView = (props) => {
    const {product,selectedItems}=props;
    console.log(props);
const [quantity,setquantity]=useState(product.quantity);
const [selectedproduct,setselectedproduct]=useState(product);
const [total,settotal]=useState(product.price*quantity);
const [checked,setchecked]=useState(false);
    const handleselectedProduct = (e,product ) => {
        product.total=quantity*product.price;
        e.target.checked?selectedItems.push(product) && props.selected(selectedItems):
            props.selected(selectedItems.filter(p=>p!==product));

    };
    return (
        <div key={product._id} className=" col-lg-12 mt-3">
            <div className="card" key={product._id}>
                <div className="card-body">
                    <div className="row  ">

                        <div className="col-2 ">
                            <img
                                alt={product.title}
                                src={`http://localhost:3001/${product.image}`}
                                className="w-75 responsive border-6"
                            />
                        </div>
                        <div className="col">
                            <div className="row mb-2">
                                <div className="col">
                                    <div className="w-40 w-sm-100">
                                        <p className="list-item-heading mb-1 truncate">
                                            {product.title}
                                        </p>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="w-15 w-sm-100">
                                        <Badge color={'primary'} pill>
                                            {product.category.title}
                                        </Badge>
                                    </div>
                                </div>
                                <div className="col ">
                                    <div className="w-15 w-sm-100 float-right">
                                        <Input type="checkbox"
                                               id="myCheck"
                                               onChange={(e)=> {handleselectedProduct(e,product)}}/>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-5 ">
                                    <p className="mb-1 float-right pt-2  text-black-50">
                                        {product.price + " DT/Piece"}
                                    </p>
                                </div>
                                <div className="col-lg-2 ">
                                    <div className="float-right">
                                    <p className="  text-black-50 mt-2 ">{"Quantity : "}</p>
                                    </div>
                                </div>
                                <div className="col-lg-2">
                                    <Input type="number" className="w-100 float-right"
                                           min={1}
                                           value={quantity}
                                           onChange={(val) => {setquantity(val.target.value); settotal(val.target.value*product.price);
                                           }}
                                    />
                                </div>
                                <div className="col-lg-3">
                                    <p className="mt-2 text-black-50">Total : {total}</p>
                                </div>
                            </div>


                        </div>
                    </div>


                </div>

            </div>
        </div>


    );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(ThumbListView);
