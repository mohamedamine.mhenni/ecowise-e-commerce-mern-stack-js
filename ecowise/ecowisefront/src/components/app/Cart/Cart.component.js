import React, {Fragment, useState} from 'react';
import {connect} from "react-redux";
import ThumbListView from "../../common/ThumbListView";
import {Button} from "reactstrap";
import {addorder} from "../../../redux/Order/actions";

const CartComponent = (props) => {
    const [selectedorderdetails,setselectedorderdetails]=useState([]);
    const [orderTotal,setorderTotal]=useState(0);
    const handlePlaceOrder=()=>{
       let ordredproducts=selectedorderdetails.map(o=>{return {productId:o._id,quantity:o.quantity,total:o.total}});
       console.log(ordredproducts);
        props.addorder('5f8312aaf098c82194bf7960',ordredproducts,'New',orderTotal);
    };
    const handleselectorderDetails=(items)=>{
        console.log(items);
        setselectedorderdetails(items);
        setorderTotal(items.reduce(function (total, currentValue) {
            return total + currentValue.total;
        }, 0));
    };
    return !props.selectedProducts[0]?
        <div className="constainer ">
            <div className="card mt-3">
                <div className="card-body">
                    <div className="d-flex align-items-center flex-column ">
                        <h3>Your shopping cart is empty.</h3>
                        <p className="small text-black-50">If you already have an account, sign in to see your cart.</p>
                    </div>
                </div>
            </div>
        </div>:(
        <div className="container">
            <div className="row">
                <div className="col-lg-9">
                    <div className="card mt-3 ">
                        <div className="card-header">
                            Selected Items
                        </div>
                        <div className="card-body">
                            <div className="row ">
                                {
                                    props.selectedProducts.map(p => {
                                        return (
                                            <ThumbListView
                                                key={p._id}
                                                product={p}
                                                selected={handleselectorderDetails}
                                                selectedItems={selectedorderdetails}
                                            />
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 ">
                    <div className="card mt-3">
                        <div className="card-body">
                        <Button className="w-100" onClick={()=>handlePlaceOrder()}>Place your order</Button>
                            <p className="small text-center">By placing your order,you agree to EcoWise.com privicy notice and condition of use</p>
                            <hr/>
                            <h6>Order Summary</h6>
                            Items : {selectedorderdetails.length}
                            <hr/>
                            <h6>Order Total:{orderTotal}</h6>
                        </div>
                    </div>
                </div>
            </div>




        </div>


    );
};

function mapStateToProps(state) {
    const {selectedProducts} = state.product;
    return {
        selectedProducts
    }
}

export default connect(mapStateToProps,{addorder})(CartComponent);
