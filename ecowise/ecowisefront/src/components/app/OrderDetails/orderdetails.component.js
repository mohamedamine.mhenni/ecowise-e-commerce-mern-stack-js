/*
import React, {Fragment, useEffect, useState} from 'react';
import {connect} from 'react-redux';
//import {deleteorderdetails, getallorderdetails, selectorderdetails} from "../../redux/orderDetails/actions";
import {Button, Container, Row, Table} from "reactstrap";
import AddNewProductModal from "../../container/product/AddNewProductModal";
import UpdateProductModal from "../../container/product/UpdateProductModal";


const OrderDetailsComponent = props => {
    const [addModalOpen, setaddModalOpen] = useState(false);
    const [updateModalOpen, setupdateModalOpen] = useState(false);
    useEffect(() => {
        props.getallorderdetails();
    }, []);
    const toggleModal = () => {
        setaddModalOpen(!addModalOpen);
    };
    const updatetoggleModal = () => {
        setupdateModalOpen(!updateModalOpen);
    };
    const handleselectproduct = (orderdetails) => {
        updatetoggleModal();
        props.selectorderdetails(orderdetails);

    };
    const deleteProduct = (id) => {
        props.deleteproduct(id);
    };
    return props.isLoading ? (<div className="loading"/>) : (
        <Fragment>

            <Row className="mb-4 justify-content-end">
                <Button color="primary" size="lg" onClick={toggleModal}>Add OrderDetails</Button>
            </Row>


            <Container>
                <Row>
                    <div className="col-lg-12 col-md-8">
                        <Table className="mb-5" hover={true} responsive={true}>
                            <thead>
                            <tr>
                                <th>Order</th>
                                <th>Product</th>
                                <th>Total</th>
                                <th>Update</th>
                                <th>Delete</th>

                            </tr>
                            </thead>
                            <thead>
                            {
                                props.orderDetailsList.map((o, i) => {
                                    return (

                                        <tr key={i}>
                                            <td>{o.orderId._id}</td>
                                            <td>{o.productId.title}</td>
                                            <td>{o.total}</td>
                                            <td><Button onClick={() => handleselectproduct(o)}>Update</Button></td>
                                            <td><Button onClick={() => deleteProduct(o._id)}>Delete</Button></td>
                                        </tr>
                                    )
                                })
                            }
                            </thead>
                        </Table>
                    </div>

                </Row>
            </Container>
            <AddNewProductModal openModal={addModalOpen} toggleModal={toggleModal}/>
            { props.selectedOrderDetails.length>0?(
                <UpdateProductModal openModal={updateModalOpen} toggleModal={updatetoggleModal}/>):null}
        </Fragment>
    );
};

function mapStateToProps(state) {
    const {isLoading, orderDetailsList, selectedOrderDetails, error} = state.orderdetails;
    console.log(state);
    return {
        isLoading, orderDetailsList, selectedOrderDetails, error
    }
}

export default connect(mapStateToProps, {deleteorderdetails, getallorderdetails, selectorderdetails})(OrderDetailsComponent);
*/
