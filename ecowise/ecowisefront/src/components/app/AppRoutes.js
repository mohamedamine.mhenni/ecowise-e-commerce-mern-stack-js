import React from 'react';
import {Redirect, Route, Switch, withRouter} from "react-router-dom";
import Product from "./Product/product.component.js";
import Category from "./Category/category.component.js";
import Productdetails from "./Product/productdetails";
import CartComponent from "./Cart/Cart.component";
import OrderComponent from "./Order/order.component";
import OrderDetails from "./Order/orderdetails.component";

import MainLayout from "./MainLayout";
const AppRoutes = ({ match }) => {
    console.log("appcomponent");
console.log(match);
    return (
        <MainLayout>
        <Switch>
            <Redirect exact from={`${match.url}/`} to={`${match.url}/product`} />
            <Route  path={`${match.url}/product`} component={Product} />
            <Route  path={`${match.url}/category`} component={Category} />
            <Route  path={`${match.url}/orderdetails/:idorder`} component={OrderDetails} />
            <Route  path={`${match.url}/productdetails/:idproduct`} component={Productdetails} />
            <Route  path={`${match.url}/cart`} component={CartComponent} />
            <Route  path={`${match.url}/orders`} component={OrderComponent} />
        </Switch>
        </MainLayout>
    );
};

export default (AppRoutes);
