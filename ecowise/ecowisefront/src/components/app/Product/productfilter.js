import React, {useEffect, useState} from 'react';
import {Button, Label} from "reactstrap";
import Select from "react-select";
import CustomSelectInput from "../../common/CustomSelectInput";
import {connect} from 'react-redux';
import {getallcategories} from '../../../redux/category/actions'
import {RangeTooltip} from "../../common/SliderPriceRange";
import ReactAutoSuggest from "../../common/ReactAutoSuggest";
import {productfilter} from "../../../redux/product/actions";

const Productfilter = (props) => {
    const [category, setcategory] = useState('');
    const [marque, setmarque] = useState('');
    const [price, setprice] = useState(null);
    const [productTitle, setproductTitle] = useState(null);
    const data = props.productsList.map(item => {
        return {name: item.title}
    });
    useEffect(() => {
        props.getallcategories();
    }, []);
    return props.isLoading ? (<div className="loading"/>) : (

        <div className="card ">
            <div className="card-body">
                <div className="row float-right ">
                    <a className="btn"
                       onClick={() => props.productfilter({title: null, pricerange: null, category: null})}>Clear all
                        filters</a>
                </div>
                <br/><br/>
                <Label>
                    category
                </Label>
                <Select
                    components={{Input: CustomSelectInput}}
                    className="react-select mb-3"
                    classNamePrefix="react-select"
                    name="form-field-name"
                    options={props.categories.map((c, i) => {
                        return {label: c.title, value: c._id, key: i};
                    })}
                    value={{label: category, value: category, key: category}}
                    onChange={val => {
                        setcategory(val.label);
                        props.productfilter({title: productTitle, pricerange: price, category: val.label});
                    }}
                />

                <Label>
                    Marque
                </Label>
                <Select
                    components={{Input: CustomSelectInput}}
                    className="react-select mb-3"
                    classNamePrefix="react-select"
                    name="form-field-name"
                    options={props.categories.map((c, i) => {
                        return {label: c.title, value: c._id, key: i};
                    })}
                    value={marque}
                    onChange={val => {
                        setmarque(val.label);
                    }}
                />

                <Label>
                    Product name
                </Label>
                <ReactAutoSuggest
                    placeholder={"Product Name"}
                    data={data}
                    onChange={value => {
                        setproductTitle(value);
                        props.productfilter({title: value, pricerange: price, category: category});
                    }}
                />

                <label className="mt-3">
                    Price range
                </label>
                <RangeTooltip
                    min={0}
                    max={1500}
                    className="mb-5"
                    defaultValue={[800, 1200]}
                    allowCross={false}
                    pushable={100}
                    onChange={val => {
                        setprice(val);
                        props.productfilter({title: productTitle, pricerange: val, category: category})
                    }
                    }
                />

                <Button onClick={() => {
                    let filter = {category: category, pricerange: price, title: productTitle};
                }}>Click</Button>
            </div>
        </div>

    );
};

function mapStateToProps(state) {
    const {categories, isLoading} = state.category;
    const {productsList} = state.product;
    return {
        categories, isLoading, productsList
    }
}

export default connect(mapStateToProps, {getallcategories, productfilter})(Productfilter);

