import React, {Fragment, useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {deleteproduct, getallproducts, selectproduct} from "../../../redux/product/actions";
import {Button, Container, Row, Table,Label} from "reactstrap";
import AddNewProductModal from "../../../container/product/AddNewProductModal";
import UpdateProductModal from "../../../container/product/UpdateProductModal";
import Productfilter from "../../../components/app/Product/productfilter";
import ImageListView from "../../../components/common/ImageListView";



const ProductComponent = props => {
    const [addModalOpen, setaddModalOpen] = useState(false);
    const [updateModalOpen, setupdateModalOpen] = useState(false);
    useEffect(() => {
        console.log(props.productsList);
        props.getallproducts();
        console.log(props.productsList);
    }, []);
    const toggleModal = () => {
        setaddModalOpen(!addModalOpen);
    };
    const updatetoggleModal = () => {
        setupdateModalOpen(!updateModalOpen);
    };
    const handleselectproduct = (product) => {
        updatetoggleModal();
        props.selectproduct(product);

    };
    const deleteProduct = (id) => {
        props.deleteproduct(id);
    };
    return props.isLoading ? (<div className="loading"/>) : (
        <div className="container-fluid mt-3">
            <AddNewProductModal openModal={addModalOpen} toggleModal={toggleModal}/>
            {props.selectedProducts.length > 0 ? (
                <UpdateProductModal openModal={updateModalOpen} toggleModal={updatetoggleModal}/>) : null}
            <div className="card mb-2 pl-2">

                    <h5>Product</h5>

            </div>
            <div className="row">
                <div className="col-lg-3" >
                    <Productfilter/>
                </div>
                <div className="col-lg-9">
                    <div className="row">
                        {props.productsItems.map(product => {

                            return (
                                <ImageListView
                                    key={product._id}
                                    product={product}

                                />
                            );

                        })}{" "}
                    </div>
                </div>
            </div>


        </div>
    );
};

function mapStateToProps(state) {
    const {isLoading, productsList,productsItems, selectedProducts, error} = state.product;
    console.log(state);
    return {
        isLoading, productsList, selectedProducts, productsItems,error
    }
}

export default connect(mapStateToProps, {getallproducts, deleteproduct, selectproduct})(ProductComponent);
