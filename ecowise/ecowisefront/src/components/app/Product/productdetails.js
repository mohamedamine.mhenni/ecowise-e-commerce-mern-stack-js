import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import {Button, Input} from 'reactstrap';
import {connect} from "react-redux";
import {getallproducts, selectproduct} from "../../../redux/product/actions";
import SliderImage from "react-zoom-slider";
import ProductService from "../../../Services/Product/ProductService";

const data1 = [{
    image: `${process.env.PUBLIC_URL}/img/img.jpg`,
    text: 'img1'
}];

const Productdetails = (props) => {
    const {idproduct} = useParams();
    const [product, setproduct] = useState(null);
    const [quantity,setquantity]=useState(1);
    const handleselectedProduct = (product ) => {
            product.quantity=quantity;
            props.selectproduct(product);
    };

    useEffect(() => {

        ProductService.getproductById(idproduct).then((response) => {
            console.log(response);
            setproduct(response);
        });

    }, []);
    let data;
    return !product ? (<div className="loading"/>) : (

        <div className="container-fluid">
            {console.log(product)}
            <div className="card">
                <div className="card-body">
                    <div className="row mb-2">
                        <div className="col-lg-5">

                            <SliderImage
                                data={data = product.pictures.length > 0 ? product.pictures.map((p, i) => {
                                    return {image: `http://localhost:3001/${p.filename}`, text: "image " + i}
                                }) : data1}
                                width="400px"
                                showDescription={true}
                                direction="right"
                            />
                        </div>
                        <div className="col-lg-4">
                            <h5>Marque : Sumsung</h5>
                            <h4>{product.title}</h4>
                            <hr/>
                            <p>category : {product.category.title}</p>
                            <p>price : {product.price} DT</p>
                            <p>description : {product.description}</p>
                            <p>Quantite :</p>
                            <Input type="number" className="w-25"
                                   min={1}
                                   defaultValue={1}
                                   onChange={(val) => {setquantity(val.target.value)
                                   }}
                            />

                        </div>
                        <div className="col-lg-3">
                            <div className="card">
                                <div className="card-body">
                                    <h6>click to add to cart</h6>
                                    <Button className="btn" onClick={() => handleselectedProduct(product)}>Add to
                                        Cart</Button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            {/* <div className="row">
                <div className="card w-100  mt-2">
                    <div className="card-header "><h4>Similare products</h4></div>
                </div>
                <div className="card-body">
                    <div className="row">
                        {props.productsList.map(product => {

                            return (
                                <ImageListView
                                    key={product._id}
                                    product={product}

                                />
                            );

                        })}{" "}
                    </div>
                </div>
            </div>*/}
        </div>

    );
};

function mapStateToProps(state) {
    const {isLoading, productsList, selectedProducts} = state.product;
    return {
        isLoading, productsList, selectedProducts
    }
}

export default connect(mapStateToProps, {getallproducts, selectproduct})(Productdetails);
