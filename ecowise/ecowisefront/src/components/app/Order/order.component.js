import React, {useEffect} from 'react';
import OrderListView from "../../common/OrderListView";
import {connect} from "react-redux";
import {getallorders} from "../../../redux/Order/actions";
import OrderFilter from "./orderfilter";

const OrderComponent = (props) => {
    useEffect(()=>{
        props.getallorders();
    },[]);
    return props.isLoading ? (<div className="loading"/>) :(
        <div className="container">
            <div className="row">
                <div className="col-lg-4">
                    <OrderFilter/>
                </div>
                <div className="col-lg-8">
                {props.orderList.map((o,i)=>{
                    return (
                        <OrderListView
                            key={i}
                            order={o}
                        />
                    )
                })}
                </div>
            </div>
        </div>
    );
};
function mapStateToProps(state) {
    const {selectedOrder,orderList,isLoading}=state.order;
    return {
        selectedOrder,
        orderList,
        isLoading
    }
}
export default connect(mapStateToProps,{getallorders}) (OrderComponent);
