import React, {useEffect, useState} from 'react';
import {Button, CustomInput, Label} from "reactstrap";
import Select from "react-select";
import CustomSelectInput from "../../common/CustomSelectInput";
import {connect} from 'react-redux';
import {getallcategories} from '../../../redux/category/actions'
import {getallproducts, productfilter} from '../../../redux/product/actions'
import {RangeTooltip} from "../../common/SliderPriceRange";
import ReactAutoSuggest from "../../common/ReactAutoSuggest";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const OrderFilter = (props) => {
    const [category, setcategory] = useState('');
    const [marque, setmarque] = useState('');
    const [price, setprice] = useState(null);
    const [productTitle, setproductTitle] = useState(null);
    const [startDateRange, setstartDateRange] = useState(null);
    const [endDateRange, setendDateRange] = useState(null);

    useEffect(() => {
        props.getallcategories();
        props.getallproducts();
    },[]);
    return props.isLoading ? (<div className="loading"/>) : (

        <div className="card mt-3">
            <div className="card-body">
                <div className="row float-right ">
                    <a className="btn"
                       onClick={() => props.productfilter({title: null, pricerange: null, category: null})}>Clear all
                        filters</a>
                </div>
                <br/><br/>
                <Label>
                    Date
                </Label>
                <CustomInput
                    type="checkbox"
                    id="exCustomCheckbox"
                    label="Today"
                />

                <div className="row mt-1 mb-2">
                    <div className="col-6">
                        <DatePicker
                            selected={startDateRange}
                            selectsStart
                            startDate={startDateRange}
                            endDate={endDateRange}
                            onChange={(e) => setstartDateRange(e)}
                            placeholderText={"Start Date"}
                        />
                    </div>
                    <div className="col-6">
                        <DatePicker
                            selected={endDateRange}
                            selectsEnd
                            startDate={startDateRange}
                            endDate={endDateRange}
                            onChange={(e) => setendDateRange(e)}
                            placeholderText={"End Date"}
                        />
                    </div>
                </div>
                <Label>
                    category
                </Label>
                <Select
                    components={{Input: CustomSelectInput}}
                    className="react-select mb-3"
                    classNamePrefix="react-select"
                    name="form-field-name"
                    options={props.categories.map((c, i) => {
                        return {label: c.title, value: c._id, key: i};
                    })}
                    value={{label: category, value: category, key: category}}
                    onChange={val => {
                        setcategory(val.label);
                        // props.productfilter({title: productTitle, pricerange: price, category: val.label});
                    }}
                />

                <Label>
                    Marque
                </Label>
                <Select
                    components={{Input: CustomSelectInput}}
                    className="react-select mb-3"
                    classNamePrefix="react-select"
                    name="form-field-name"
                    options={props.categories.map((c, i) => {
                        return {label: c.title, value: c._id, key: i};
                    })}
                    value={marque}
                    onChange={val => {
                        setmarque(val.label);
                    }}
                />

                <Label>
                    Product name
                </Label>
                <ReactAutoSuggest
                    placeholder={"Product Name"}
                    data={props.productsList.map(item => {
                        return {name: item.title}
                    })}
                    onChange={value => {
                        setproductTitle(value);
                    }}
                />

                <label className="mt-3">
                    Price range
                </label>
                <RangeTooltip
                    min={0}
                    max={1500}
                    className="mb-5"
                    defaultValue={[800, 1200]}
                    allowCross={false}
                    pushable={100}
                    onChange={val => {
                        setprice(val);
                        props.productfilter({title: productTitle, pricerange: val, category: category})
                    }
                    }
                />

                <br/>
                <Button onClick={() => {
                    let filter = {category: category, pricerange: price, title: productTitle};
                }}>Click</Button>
            </div>
        </div>

    );
};

function mapStateToProps(state) {
    const {categories, isLoading} = state.category;
    const {productsList} = state.product;
    return {
        categories, isLoading, productsList
    }
}

export default connect(mapStateToProps, {getallcategories, getallproducts, productfilter})(OrderFilter);

