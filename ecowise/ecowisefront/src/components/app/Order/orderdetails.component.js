import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import {Table,Button} from "reactstrap";
import OrderDetailsService from "../../../Services/Order/OrderDetailsService";


const Orderdetails= () => {
    const {idorder} = useParams();
    const [orderdetailsList,setorderdetailsList]=useState(null);
    const  getOrder=(id)=>{
        OrderDetailsService.getAllOrderDetails(id).then((data)=>{
            setorderdetailsList(data);
        })
    };
    useEffect(()=>{
        getOrder(idorder);
    },[]);
    return !orderdetailsList?<div className="loading"/>: (
        <div className="container">
            <div className="card mt-3">
                <div className="card-body">
                    <div className="d-flex justify-content-between">
                    <h5>Order Details</h5>
                    <Button onClick={()=>window.print()}>Print</Button>
                    </div>
                </div>
            </div>
            <div className="card">
                <div className="card-body">
                    {orderdetailsList.map((o,i)=>{
                        return (
                            <div className="card mt-2">
                                <div className="card-body">
                                    <div className="d-flex  align-items-center justify-content-between">
                                        <a className=" small text-black-50 ">order ID : {o.orderId._id} </a>
                                        <a className=" small text-black-50 "> OrderDate: {o.orderId.createdDate} </a>
                                    </div>
                                    <hr/>

                                <div className="row">

                                    <div className="col-lg-6">
                                        <p>Supplier</p>
                                        <h6>{o.productId.owner.firstname}</h6>
                                        <p className="small text-black-50">Cantact Name : {o.productId.owner.username}</p>
                                        <p className="small text-black-50">Email : {o.productId.owner.email}</p>
                                        <p className="small text-black-50">Campany name : astiorls campany</p>
                                        <p className="small text-black-50">Adress : {o.productId.owner.adress}</p>
                                        <p className="small text-black-50">Tel  : +158 5889663325</p>
                                    </div>
                                    <div className="col-lg-6 ">
                                        <p>Buyer</p>
                                        <h6>{o.productId.owner.firstname}</h6>
                                        <p className="small text-black-50">Cantact Name : {o.productId.owner.username}</p>
                                        <p className="small text-black-50">Email : {o.productId.owner.email}</p>
                                    </div>

                                </div>
                                <hr/>
                                <div className="row">

                                    <p>Product Details</p>
                                    <Table>
                                        <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>Product Name</th>
                                            <th>Quantity</th>
                                            <th>Unit</th>
                                            <th>Unit price</th>
                                            <th>Total</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{i}</td>
                                            <td>{o.productId.title}</td>
                                            <td>{o.quantity}</td>
                                            <td>piece</td>
                                            <td>{o.productId.price+" DT"}</td>
                                            <td>{o.total +"DT"}</td>
                                        </tr>
                                        </tbody>
                                    </Table>
                                </div>
                                </div>
                            </div>
                        );
                    })}

                </div>
            </div>
        </div>
    );
};

export default Orderdetails;
