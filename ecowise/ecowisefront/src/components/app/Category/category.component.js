import React, {Fragment, useEffect, useState} from 'react';
import {getallcategories,selectcategories,deletecategory} from "../../../redux/category/actions";
import {Button, Container, Row, Table} from "reactstrap";
import {connect} from "react-redux";
import UpdateCategoryModal from "../../../container/category/UpdateCategoryModal";
import AddNewCategoryModal from "../../../container/category/AddNewCategoryModal";
const CategoryComponent = (props) => {
    const {categories}=props;
    const [openaddModal,setopenaddModal]=useState(false);
    const [openupdateModal,setopenupdateModal]=useState(false);
    useEffect(()=>{
        props.getallcategories()
        },[]
    );
    const toggleAddModal=()=>{
        setopenaddModal(!openaddModal);
    };
    const toggleUpdateModal=()=>{
        setopenupdateModal(!openupdateModal);
    };
    const handleDeleteCategory=(categoryId)=>{
        props.deletecategory(categoryId);
    };
    const handleSelectCategories=(category)=>{
        props.selectcategories(category);
        toggleUpdateModal();
    };
    return props.isLoading ? (<div className="loading"/>) : (
        <Fragment>
            <div className="container">
                <div className="row">
                    <div className="col-lg-8">
                        <h1>Categories</h1>
                    </div>
                    <div className="col-lg-4 ">
                        <Button className="btn btn-primary float-right" onClick={()=>toggleAddModal()}>Add New Category</Button>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        <Table responsive hover={true}>
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Parent</th>
                                    <th>Delete</th>
                                    <th>Update</th>
                                </tr>
                            </thead>
                            <tbody>
                            {
                                categories.map((c,i)=> {return(
                                    <tr key={i}>
                                        <td>{c.title}</td>
                                        <td>{c.description}</td>
                                        <td>{c.parent?c.parent.title:'No parent'}</td>
                                        <td><Button className="btn btn-danger" onClick={()=>handleDeleteCategory(c._id)}>Delete</Button></td>
                                        <td><Button className="btn btn-primary" onClick={()=>handleSelectCategories(c)}>Update</Button></td>
                                    </tr>
                                )})
                            }

                            </tbody>
                        </Table>
                    </div>
                </div>

            </div>
            {props.selectedCategories.length>0?<UpdateCategoryModal openModal={openupdateModal} toggleModal={toggleUpdateModal}/>:null}
            <AddNewCategoryModal openModal={openaddModal} toggleModal={toggleAddModal}/>
        </Fragment>
    );
};
function mapStateToProps(state) {
    const {isLoading,categories,selectedCategories} =state.category;
    return {
        isLoading,categories,selectedCategories
    }
}
export default connect(mapStateToProps,{getallcategories,selectcategories,deletecategory})(CategoryComponent);
