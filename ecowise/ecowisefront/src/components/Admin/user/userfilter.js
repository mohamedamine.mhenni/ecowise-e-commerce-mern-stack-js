import React, {useState} from 'react';
import ReactAutoSuggest from "../../common/ReactAutoSuggest";
import Select from "react-select";
import CustomSelectInput from "../../common/CustomSelectInput";
import {DropdownItem, DropdownMenu, Input} from "reactstrap";
import {connect} from "react-redux";
import {userfilter} from "../../../redux/user/actions";
const Types=[
    {label: 'Client', value: 'client', key: 1},
    {label: 'Company', value: 'company', key: 2}
];
const Userfilter = (props) => {
    const [type,settype]=useState('');
    const [search,setsearch]=useState(null);
    return (
        <div className="row">
            <div className="col">
                <Input type="text"
                       placeholder={"Search"}
                       defaultValue={search}
                       onChange={e => {
                           setsearch(e.target.value);
                           props.userfilter({role:type.value,search:e.target.value})}
                       }
                />
            </div>
            <div className="col">
                <Select
                    components={{Input: CustomSelectInput}}
                    className="react-select mb-3"
                    classNamePrefix="react-select"
                    placeholder={"Select role"}
                    name="form-field-name"
                    options={Types}
                    value={type}
                    onChange={val => {settype(val);props.userfilter({role:val.value,search:search})}}
                />
            </div>
        </div>
    );
};

export default connect(null,{userfilter})(Userfilter);
