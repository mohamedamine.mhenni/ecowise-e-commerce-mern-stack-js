import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {addnewuser, deleteuser, getallusers, selectusers} from "../../../redux/user/actions";
import {Button, Table} from "reactstrap"
import AddNewUserModal from "../../../container/user/AddNewUserModal";

import UpdateUserModal from "../../../container/user/UpdateUserModal";
import Userfilter from "./userfilter";

const UsersAdmin = props => {

    const [openaddModal, setopenaddModal] = useState(false);
    const [openupdateModal, setopenupdateModal] = useState(false);
    useEffect(() => {
        props.getallusers();
        // eslint-disable-next-line import/no-extraneous-dependencies
    }, []);

    const toggleaddModal = () => {
        console.log('toggle');
        setopenaddModal(!openaddModal);
    };
    const toggleupdateModal = () => {
        console.log('toggle update');
        setopenupdateModal(!openupdateModal);
    };
    const handleDeleteUser = (id) => {
        props.deleteuser(id);
    };
    const handleUpdateUser = (u) => {
        props.selectusers(u);
        toggleupdateModal();
    };
  {/*  */}

    return props.isLoading ? (<div className="loading"/>) : (

        <div className="container-fluid mt-3">
            <div className="row">
                <div className="col-lg-12">
                    <div className="card">
                        <div className="card-header">
                            <p className="mb-2 small text-black-50">Advanced options</p>
                            <Userfilter/>
                        </div>
                        <div className="card-body">
                            <div className="row">
                                <div className="col-lg-3">
                                    <h3>Users List</h3>
                                </div>
                                <div className="col-lg-9 ">
                                    <div className="float-right">
                                        <Button color="primary" size="md" onClick={toggleaddModal}>Add new user</Button>
                                    </div>
                                </div>

                            </div>
                            <div className="row">
                                <div className="col-lg-12 col-md-8">
                                    <Table responsive={true} hover={true}>
                                        <thead>
                                        <tr>
                                            <th>firstname</th>
                                            <th>lastname</th>
                                            <th>username</th>
                                            <th>email</th>
                                            {/* <td>password</td>*/}
                                            <th>adress</th>
                                            <th>role</th>
                                            <th>delete</th>
                                            <th>update</th>
                                        </tr>
                                        </thead>
                                        <thead>
                                        {
                                            props.usersItems.map((u, i) => {
                                                return (
                                                    <tr key={i}>
                                                        <td>{u.firstname}</td>
                                                        <td>{u.lastname}</td>
                                                        <td>{u.username}</td>
                                                        <td>{u.email}</td>
                                                        {/*<td >{u.password}</td>*/}
                                                        <td>{u.adress}</td>
                                                        <td>{u.role}</td>
                                                        <td><Button onClick={() => handleUpdateUser(u)}>Update</Button></td>
                                                        <td><Button onClick={() => handleDeleteUser(u._id)}>Delete</Button></td>
                                                    </tr>
                                                )
                                            })
                                        }
                                        </thead>
                                    </Table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            {props.selectedUsers.length > 0 ? (
                <UpdateUserModal openModal={openupdateModal} toggleModal={toggleupdateModal}/>) : null}
            <AddNewUserModal openModal={openaddModal} toggleModal={toggleaddModal}/>
        </div>


    );

};

function mapStateToProps(state) {
    const {isLoading, usersItems, selectedUsers} = state.user;

    return {isLoading, usersItems, selectedUsers};
}

export default connect(mapStateToProps, {getallusers, addnewuser, deleteuser, selectusers})(UsersAdmin);
