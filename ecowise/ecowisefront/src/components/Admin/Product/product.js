import React, {Fragment, useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {deleteproduct, getallproducts, selectproduct} from "../../../redux/product/actions";
import {Button, Container, Row, Table,Label} from "reactstrap";
import AddNewProductModal from "../../../container/product/AddNewProductModal";
import UpdateProductModal from "../../../container/product/UpdateProductModal";
import Productfilter from "../../../components/app/Product/productfilter";
import ImageListView from "../../../components/common/ImageListView";



const ProductAdmin = props => {
    const [addModalOpen, setaddModalOpen] = useState(false);
    const [updateModalOpen, setupdateModalOpen] = useState(false);
    useEffect(() => {
        console.log(props.productsList);
        props.getallproducts();
        console.log(props.productsList);
    }, []);
    const toggleModal = () => {
        setaddModalOpen(!addModalOpen);
    };
    const updatetoggleModal = () => {
        setupdateModalOpen(!updateModalOpen);
    };
    const handleselectproduct = (product) => {
        updatetoggleModal();
        props.selectproduct(product);

    };
    const deleteProduct = (id) => {
        props.deleteproduct(id);
    };
    return props.isLoading ? (<div className="loading"/>) : (
        <Fragment>

            <div className="container-fluid mt-3">

                <Row>
                    <div className="col-lg-3" >
                        <Productfilter/>
                    </div>
                    <div className="col-lg-9 col-md-8">
                        <div className="card">
                            <div className="card-body">
                                <Row className="mb-4 ">
                                    <div className="col-lg-6 ">
                                        <h4>Products List</h4>
                                    </div>
                                    <div className="col-lg-6 ">
                                        <Button className="float-right" color="primary" size="lg" onClick={toggleModal}>Add product</Button>
                                    </div>
                                </Row>
                        <Table className="mb-5" hover={true} responsive={true}>
                            <thead>
                            <tr>
                                <th>title</th>
                                <th>Owner</th>
                                <th>Category</th>
                                <th>description</th>
                                <th>price</th>
                                <th>Update</th>
                                <th>Delete</th>

                            </tr>
                            </thead>
                            <thead>
                            {
                                props.productsItems.map((p, i) => {
                                    return (

                                        <tr key={i}>
                                            <td>{p.title}</td>
                                            <td>{p.owner.firstname}</td>
                                            <td>{p.category.title}</td>
                                            <td>{p.description}</td>
                                            <td>{p.price}</td>
                                            <td><Button onClick={() => handleselectproduct(p)}>Update</Button></td>
                                            <td><Button onClick={() => deleteProduct(p._id)}>Delete</Button></td>
                                        </tr>
                                    )
                                })
                            }
                            </thead>
                        </Table>
                            </div>
                        </div>
                    </div>

                </Row>
            </div>
            <AddNewProductModal openModal={addModalOpen} toggleModal={toggleModal}/>
            {props.selectedProducts.length > 0 ? (
                <UpdateProductModal openModal={updateModalOpen} toggleModal={updatetoggleModal}/>) : null}

        </Fragment>
    );
};

function mapStateToProps(state) {
    const {isLoading, productsList,productsItems, selectedProducts, error} = state.product;
    console.log(state);
    return {
        isLoading, productsList, selectedProducts, productsItems,error
    }
}

export default connect(mapStateToProps, {getallproducts, deleteproduct, selectproduct})(ProductAdmin);
