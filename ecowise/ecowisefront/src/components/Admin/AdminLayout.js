import React from 'react';
import NavBarAdmin from "../NavBar/NavBarAdmin";

const AdminLayout = ({children}) => {
    return (
        <div>
            <NavBarAdmin/>
            <main>
                {children}
            </main>
        </div>
    );
};

export default AdminLayout;
