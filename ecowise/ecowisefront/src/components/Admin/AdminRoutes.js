import React from 'react';
import {Redirect, Route, Switch, withRouter} from "react-router-dom";
import ProductAdmin from "./Product/product";
import CategoryAdmin from "./Category/category";
import UsersAdmin from "./user/user.component";


import AdminLayout from "./AdminLayout";
const AdminRoutes = ({ match }) => {
    console.log("admin routes");
console.log(match);
    return (
        <AdminLayout>
        <Switch>
            <Redirect exact from={`${match.url}/`} to={`${match.url}/users`} />
            <Route  path={`${match.url}/product`} component={ProductAdmin} />
            <Route  path={`${match.url}/category`} component={CategoryAdmin} />
            <Route  path={`${match.url}/users`} component={UsersAdmin} />
        </Switch>
        </AdminLayout>
    );
};

export default (AdminRoutes);
