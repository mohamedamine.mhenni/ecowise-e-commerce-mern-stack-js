import React from 'react';
import {Link} from "react-router-dom";

const NavBar = (props) => {

    return (
        <nav className="navbar navbar-expand navbar-dark bg-dark">
            <Link to={"/"} className="navbar-brand">
                ECOWISE
            </Link>
            <div className="navbar-nav mr-auto">
                <li className="nav-item">
                    <Link to={`/admin/users`} className="nav-link">
                        Users
                    </Link>
                </li>
                <li className="nav-item">
                    <Link to={'/admin/product'} className="nav-link">
                        Product
                    </Link>
                </li>
                <li className="nav-item">
                    <Link to={"/admin/category"} className="nav-link">
                        Categories
                    </Link>
                </li>
            </div>
                <div className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <Link to={"/login"} className="nav-link">
                            Login
                        </Link>
                    </li>

                    <li className="nav-item">
                        <Link to={"/register"} className="nav-link">
                            Sign Up
                        </Link>
                    </li>
                </div>

        </nav>
    );
};

export default NavBar;
