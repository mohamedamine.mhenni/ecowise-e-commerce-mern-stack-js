import React, {Fragment, useState} from 'react';
import {Input,Button} from "reactstrap";
import {connect} from "react-redux";
import {register} from '../../redux/auth/actions';
import{history} from "../../helpers/history";
import {NavLink} from "react-router-dom";

const SignupComponent = (props) => {

    const [firstname, setfirstname] = useState('');
    const [lastname, setlastname] = useState('');
    const [username, setusername] = useState('');
    const [email, setemail] = useState('');
    const [password, setpassword] = useState('');
    const [adress, setadress] = useState('');
    const [country, setcountry] = useState('');
    const [city, setcity] = useState('');
    const [phone, setphone] = useState('');
    const [companyname, setcompanyname] = useState('');
    const [companydescription, setcompanydescription] = useState('');
    const [registrationtype,setregistrationtype]=useState('client');

    const  handleRegister=async  () =>{
         props.register(firstname,lastname,username,country,city,companyname,companydescription,phone,password,email,adress,registrationtype).then(
             ()=>{
                 props.history.push('/');
             }
         );


    };

    return (
        <Fragment>
        <div className="fixed-background">
            <div className="container h-100 w-100 align-items-center">
                <div className="row h-100 justify-content-center align-items-center">
                    <div className="card  ">
                        <div className="card-body">

                            <div className="row">
                                <div className="col-lg-6">
                                    <div className="mb-5 justify-content-center">
                                        <img alt={"eee"} src={process.env.PUBLIC_URL+'/img/ecowiselogo.png'} className="  w-50"/>
                                    </div>
                                    <p className=" h2">MAGIC IS IN THE DETAILS</p>
                                    <p className="white mb-0">
                                        Please use this form to register. <br/>
                                        If you are a member, please{" "}
                                        <NavLink to={`/login`} className="white">
                                            login
                                        </NavLink>
                                        .
                                    </p>

                                </div>
                                <div className="col-lg-6 scroll-card ">
                                    <div className="d-flex align-items-center mb-5 justify-content-between">
                                        <Button className="btn btn-primary rounded-pill" data-toggle="button" aria-pressed="false"
                                                onClick={()=>{setregistrationtype('client')}}>
                                                {"Client"}
                                        </Button>{" "}
                                        <Button className="btn btn-primary rounded-pill" data-toggle="button" aria-pressed="false"
                                                onClick={()=>{setregistrationtype('company')}}>
                                                {"Company"}
                                        </Button>{" "}
                                    </div>
                                    {registrationtype==="client"?
                                    <div className="input-group mb-3">
                                        <Input type="name" placeholder={"First Name"} defaultValue={firstname}
                                               onChange={event => {
                                                   setfirstname(event.target.value);
                                               }}/>
                                    </div>:null}
                                    {registrationtype==="client"?
                                    <div className="input-group mb-3">
                                        <Input type="name" placeholder={"Last Name"} defaultValue={lastname}
                                               onChange={event => {
                                                   setlastname(event.target.value);
                                               }}/>
                                    </div>:null}
                                    {registrationtype==="client"?
                                    <div className="input-group mb-3">
                                        <Input type="name" placeholder={"User Name"} defaultValue={username}
                                               onChange={event => {
                                                   setusername(event.target.value);
                                               }}/>
                                    </div>:null}
                                    {registrationtype==="company"?
                                    <div className="input-group mb-3">
                                        <Input type="name" placeholder={"Company  Name"} defaultValue={firstname}
                                               onChange={event => {
                                                   setfirstname(event.target.value);
                                               }}/>
                                    </div>:null}
                                    <div className="input-group mb-3">
                                        <Input type="password" placeholder={"Password"} defaultValue={password}
                                               onChange={event => {
                                                   setpassword(event.target.value);
                                               }}/>
                                    </div>
                                    <div className="input-group mb-3">
                                        <Input type="email" placeholder={"Email"} defaultValue={email}
                                               onChange={event => {
                                                   setemail(event.target.value);
                                               }}/>
                                    </div>
                                    <div className="input-group mb-3">
                                        <Input type="name" placeholder={"Adress"} defaultValue={adress}
                                               onChange={event => {
                                                   setadress(event.target.value);
                                               }}/>
                                    </div>
                                    <div className="input-group mb-3">
                                        <Input type="name" placeholder={"Country"} defaultValue={country}
                                               onChange={event => {
                                                   setcountry(event.target.value);
                                               }}/>
                                    </div>
                                    <div className="input-group mb-3">
                                        <Input type="name" placeholder={"City"} defaultValue={city}
                                               onChange={event => {
                                                   setcity(event.target.value);
                                               }}/>
                                    </div>
                                    <div className="input-group mb-3">
                                        <Input type="name" placeholder={"Phone"} defaultValue={phone}
                                               onChange={event => {
                                                   setphone(event.target.value);
                                               }}/>
                                    </div>
                                    {registrationtype==="company"?
                                    <div className="input-group mb-3">
                                        <Input type="textarea" placeholder={"Company  Description"}
                                               defaultValue={companydescription}
                                               onChange={event => {
                                                   setcompanydescription(event.target.value);
                                               }}/>
                                    </div>:null}
                                    <div className="input-group mb-3">
                                        <Button className="btn-block btn btn-primary" onClick={()=>{
                                            handleRegister()
                                        }}>Register</Button>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        </Fragment>
    );
};
function mapStateToProps(state) {
    const {registred,isLoggedIn}=state.auth;
    return {
        registred,
        isLoggedIn
    }
}
export default connect(mapStateToProps,{register}) (SignupComponent);
