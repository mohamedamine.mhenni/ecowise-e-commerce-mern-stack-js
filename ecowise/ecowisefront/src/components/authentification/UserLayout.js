import React from 'react';
import NavBar from "../NavBar/NavBar";

const UserLayout = ({ children }) => {
    return (
        <div>
            <NavBar/>
            <main>
            {children}
            </main>
        </div>
    );
};

export default UserLayout;

