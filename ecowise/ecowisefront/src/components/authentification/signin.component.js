import React, {Fragment, useState} from 'react';
import {Button, Input} from "reactstrap";
import {connect} from "react-redux";
import {login} from '../../redux/auth/actions';
import {NavLink} from "react-router-dom";


const SigninComponent = (props) => {


    const [email, setemail] = useState('');
    const [password, setpassword] = useState('');


    const handleLogin = () => {
        props.login(email, password).then(()=>props.history.push("/app"));
    };

    return (
        <Fragment>
            <div className="fixed-background">
                <div className="container h-100 w-100 align-items-center">
                    <div className="row h-100 justify-content-center align-items-center">
                        <div className="card  ">
                            <div className="card-body">

                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="mb-5 justify-content-center">
                                            <img alt={"eee"} src={process.env.PUBLIC_URL + '/img/ecowiselogo.png'}
                                                 className="  w-50"/>
                                        </div>
                                        <p className=" h2">MAGIC IS IN THE DETAILS</p>
                                        <p className="white mb-0">
                                            Please use this form to sign in. <br/>
                                            If you are not a member, please{" "}
                                            <NavLink to={`/authentication/signup`} className="white">
                                                sign up
                                            </NavLink>
                                            .
                                        </p>

                                    </div>
                                    <div className="col-lg-6  w-100 h-100">
                                        <div className="justify-content-center align-items-center">
                                            <h6 className="mb-5">Login</h6>
                                            <div className="input-group mb-3">
                                                <Input type="email" placeholder={"Email"} defaultValue={email}
                                                       onChange={event => {
                                                           setemail(event.target.value);
                                                       }}/>
                                            </div>
                                            <div className="input-group mb-3">
                                                <Input type="password" placeholder={"Password"} defaultValue={password}
                                                       onChange={event => {
                                                           setpassword(event.target.value);
                                                       }}/>
                                            </div>


                                            <div className="input-group mb-3">
                                                <Button className="btn-block btn btn-primary" onClick={() => {
                                                    handleLogin()
                                                }}>Login</Button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

function mapStateToProps(state) {
    const {registred, isLoggedIn} = state.auth;
    return {
        registred,
        isLoggedIn
    }
}

export default connect(mapStateToProps, {login})(SigninComponent);
