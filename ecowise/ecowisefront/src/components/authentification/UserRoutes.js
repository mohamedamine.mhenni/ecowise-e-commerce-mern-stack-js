import React from 'react';
import { Route, Switch, Redirect } from "react-router-dom";
import SignupComponent from "./signup.component";
import SigninComponent from "./signin.component";
import UserLayout from "../authentification/UserLayout";
const UserRoutes = ({match}) => {
console.log(match);
            return (
            <UserLayout>
                <Switch>
                    <Redirect exact from={`${match.url}/`} to={`${match.url}/login`} />
                    <Route path={`${match.url}/login`} component={SigninComponent} />
                    <Route path={`${match.url}/signup`} component={SignupComponent} />
                    <Redirect to="/error" />
                </Switch>

            </UserLayout>

    );
};

export default UserRoutes;
