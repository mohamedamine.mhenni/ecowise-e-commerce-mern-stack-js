import React, {useState} from 'react';

import {connect} from "react-redux";
import {addnewcategory} from "../../redux/category/actions";
import {Button, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import Select from "react-select";
import CustomSelectInput from "../../components/common/CustomSelectInput";

const AddNewCategoryModal = props => {
    const [title, settitle] = useState('');
    const [description, setdescription] = useState('');
    const [parent, setparent] = useState(null);

    function addNewCategory() {
        console.log(parent);
        props.addnewcategory(title, description, parent ? parent.val : null);
    }

    return (

        <Modal
            isOpen={props.openModal}
            toggle={props.toggleModal}
            wrapClassName="modal-right"
            backdrop="static"
        >
            <ModalHeader>
                Add New Category
            </ModalHeader>
            <ModalBody>

                <Label>title</Label>
                <Input type="text"
                       defaultValue={title}
                       onChange={e => settitle(e.target.value)}
                />


                <Label>Parent</Label>
                <Select
                    components={{Input: CustomSelectInput}}
                    className="react-select"
                    classNamePrefix="react-select"
                    name="form-field-name"
                    options={props.categoriesItems.map((c, i) => {
                        return {label: c.title, value: c._id, key: i};
                    })}
                    value={parent}
                    onChange={val => {
                        setparent(val);
                    }}
                />
                <Label>Description</Label>
                <Input type="textarea"
                       defaultValue={description}
                       onChange={e => setdescription(e.target.value)}
                />

            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={() => props.toggleModal()}>
                    Cancel
                </Button>
                <Button color="primary" onClick={() => addNewCategory()}>
                    Add New Category
                </Button>
            </ModalFooter>
        </Modal>


    );
};

function mapStateToProps(state) {
    const {categoriesItems} = state.category;
    return {
        categoriesItems
    }
}

export default connect(mapStateToProps, {addnewcategory})(AddNewCategoryModal);
