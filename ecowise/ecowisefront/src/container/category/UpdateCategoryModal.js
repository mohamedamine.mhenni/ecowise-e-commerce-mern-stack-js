import React, {useEffect, useState} from 'react';

import {connect} from "react-redux";
import {updatecategory} from "../../redux/category/actions";
import {Button, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import CustomSelectInput from "../../components/common/CustomSelectInput";
import Select from "react-select";

const UpdateCategoryModal = props => {
    const {selectedCategories,categoriesItems} = props;

    const [title, settitle] = useState(selectedCategories[0].title);
    const [description, setdescription] = useState(selectedCategories[0].description);
    const [parent, setparent] = useState(selectedCategories[0].parent ? {
        label: selectedCategories[0].parent.title,
        value: selectedCategories[0].parent._id,
        key: -1
    }:{label:'No Parent',value:'',key:1});

    useEffect(() => {
        console.log(props.categoriesItems);
    });
    const handleUpdateCategory = () => {

        props.updatecategory(selectedCategories[0]._id,title,description,parent.value===''?null:parent.value);
    };

    return (

        <Modal
            isOpen={props.openModal}
            toggle={props.toggleModal}
            wrapClassName="modal-right"
            backdrop="static"
        >
            <ModalHeader>
                Update Category
            </ModalHeader>
            <ModalBody>
                <Label>Title</Label>
                <Input type="text"
                       defaultValue={title}
                       onChange={e => settitle(e.target.value)}
                />
                <Label>Description</Label>
                <Input type="text"
                       defaultValue={description}
                       onChange={e => setdescription(e.target.value)}
                />
                <Label>Parent</Label>
                <Select
                    components={{Input: CustomSelectInput}}
                    className="react-select"
                    classNamePrefix="react-select"
                    name="form-field-name"
                    options={categoriesItems.map((c,i)=>{
                        return {label:c.title,value:c._id,key:i}
                    })}
                    value={parent}
                    onChange={val => {
                        setparent(val);
                    }}
                />
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={() => props.toggleModal()}>
                    Cancel
                </Button>
                <Button color="primary" onClick={() => handleUpdateCategory()}>
                    Update User
                </Button>
            </ModalFooter>
        </Modal>


    );
};

function mapStateToProps(state) {
    const {selectedCategories, categoriesItems} = state.category;
    return {
        selectedCategories, categoriesItems
    }
}

export default connect(mapStateToProps, {updatecategory})(UpdateCategoryModal);
