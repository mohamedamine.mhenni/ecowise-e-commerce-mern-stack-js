import React,{useState,useEffect} from 'react';

import { connect } from "react-redux";
import {addnewuser} from "../../redux/user/actions";
import {
    CustomInput,
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Input,
    Label
} from "reactstrap";

const AddNewUserModal = props => {
    const [firstname, setfirstname] = useState( "");
    const [lastname, setlastname] = useState( "");
    const [username, setusername] = useState( "");
    const [email, setemail] = useState( "");
    const [password, setpassword] = useState( "");
    const [adress, setadress] = useState( "");
    const [role, setrole] = useState( "admin");
    function addNewUser(e){
        props.addnewuser(firstname,lastname,username,password,email,adress,role);
    }
    return (

        <Modal
            isOpen={props.openModal}
            toggle={props.toggleModal}
            wrapClassName="modal-right"
            backdrop="static"
        >
            <ModalHeader>
                Add New User
            </ModalHeader>
            <ModalBody>
                <Label>First Name</Label>
                <Input type="text"
                       defaultValue={firstname}
                       onChange={e=>setfirstname(e.target.value)}
                />
                <Label>Last Name</Label>
                <Input type="text"
                       defaultValue={lastname}
                       onChange={e=>setlastname(e.target.value)}
                />
                <Label>User Name</Label>
                <Input type="text"
                       defaultValue={username}
                       onChange={e=>setusername(e.target.value)}
                />
                <Label>Email</Label>
                <Input type="text"
                       defaultValue={email}
                       onChange={e=>setemail(e.target.value)}
                />
                <Label>Password</Label>
                <Input type="text"
                       defaultValue={password}
                       onChange={e=>setpassword(e.target.value)}
                />
                <Label>Adress</Label>
                <Input type="text"
                       defaultValue={adress}
                       onChange={e=>setadress(e.target.value)}
                />
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={() => props.toggleModal()}>
                    Cancel
                </Button>
                <Button color="primary" onClick={() => addNewUser()}>
                    Add New User
                </Button>
            </ModalFooter>
        </Modal>


    );
};



export default connect(null,{addnewuser})(AddNewUserModal);
