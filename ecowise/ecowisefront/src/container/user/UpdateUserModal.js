import React,{useState,useEffect} from 'react';

import { connect } from "react-redux";
import {updateuser} from "../../redux/user/actions";
import {
    CustomInput,
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Input,
    Label
} from "reactstrap";

const UpdateUserModal = props => {
    const {selectedUsers}=props;
    const [firstname, setfirstname] = useState( selectedUsers[0].firstname);
    const [lastname, setlastname] = useState( selectedUsers[0].lastname);
    const [username, setusername] = useState( selectedUsers[0].username);
    const [email, setemail] = useState( selectedUsers[0].email);
    const [password, setpassword] = useState( selectedUsers[0].password);
    const [adress, setadress] = useState( selectedUsers[0].adress);
    const [role, setrole] = useState( selectedUsers[0].role||"admin");
    useEffect(()=>{
    });
    const handleUpdateUser=()=>{
        props.updateuser(selectedUsers[0]._id,firstname,lastname,username,password,email, adress, role);
    };

    return (

        <Modal
            isOpen={props.openModal}
            toggle={props.toggleModal}
            wrapClassName="modal-right"
            backdrop="static"
        >
            <ModalHeader>
                Update User
            </ModalHeader>
            <ModalBody>
                <Label>First Name</Label>
                <Input type="text"
                       defaultValue={firstname}
                       onChange={e=>setfirstname(e.target.value)}
                />
                <Label>Last Name</Label>
                <Input type="text"
                       defaultValue={lastname}
                       onChange={e=>setlastname(e.target.value)}
                />
                <Label>User Name</Label>
                <Input type="text"
                       defaultValue={username}
                       onChange={e=>setusername(e.target.value)}
                />
                <Label>Email</Label>
                <Input type="text"
                       defaultValue={email}
                       onChange={e=>setemail(e.target.value)}
                />
                <Label>Password</Label>
                <Input type="text"
                       defaultValue={password}
                       onChange={e=>setpassword(e.target.value)}
                />
                <Label>Adress</Label>
                <Input type="text"
                       defaultValue={adress}
                       onChange={e=>setadress(e.target.value)}
                />
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={() => props.toggleModal()}>
                    Cancel
                </Button>
                <Button color="primary" onClick={()=>handleUpdateUser()} >
                    Update User
                </Button>
            </ModalFooter>
        </Modal>


    );
};

function mapStateToProps(state) {
    const {selectedUsers} = state.user;
    return{
        selectedUsers
    }
}

export default connect(mapStateToProps,{updateuser})(UpdateUserModal);
