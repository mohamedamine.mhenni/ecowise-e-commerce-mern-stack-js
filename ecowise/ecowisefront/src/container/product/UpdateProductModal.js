import React, {useEffect, useState} from 'react';

import {connect} from "react-redux";
import {updateproduct} from "../../redux/product/actions";
import {getallusers} from "../../redux/user/actions";
import {Button, Input,FormGroup,FormFeedback,FormText,Form, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

import Select from "react-select";
import CustomSelectInput from "../../components/common/CustomSelectInput";

const UpdateProductModal = props => {
    const {selectedProducts} = props;
    const [title, settitle] = useState(selectedProducts[0].title);
    const [owner, setowner] = useState(selectedProducts[0].owner);
    const [description, setdescription] = useState(selectedProducts[0].description);
    const [category, setcategory] = useState(selectedProducts[0].category.title);
    const [price, setprice] = useState(selectedProducts[0].price);

    useEffect(() => {
        props.getallusers();
    },[]);
    const handleUpdateProduct=()=>{
        console.log(owner);
        props.updateproduct(selectedProducts[0]._id,title,owner.value,description,"5f873a690c335115f07084ab",price)
    };
    return(

        <Modal
            isOpen={props.openModal}
            toggle={props.toggleModal}
            wrapClassName="modal-right"
            backdrop="static"
        >
            <ModalHeader>
                UpdateProduct {console.log("hello")}
            </ModalHeader>
            <ModalBody>
                <Label>Title</Label>
                <Input type="text"
                       defaultValue={title}
                       onChange={e => settitle(e.target.value)}
                />
                <Label>Owner</Label>
                <Select
                    components={{ Input: CustomSelectInput }}
                    className="react-select"
                    classNamePrefix="react-select"
                    name="form-field-name"
                    options={props.users.map((u, i) => {
                        return { label: u.username, value: u._id, key: i };
                    })}
                    value={{label:owner.username,value:owner._id,key:0}}
                    onChange={val => {
                        setowner(val);
                    }}
                />

                <Label>Description</Label>
                <Input type="text"
                       defaultValue={description}
                       onChange={e => setdescription(e.target.value)}
                />

                <Label>Price</Label>
                <Input type="number"
                       defaultValue={price}
                       onChange={e => {setprice(e.target.value)}}
                />
                <Label>Category</Label>
                <Input type="text"
                       defaultValue={category}
                       onChange={e => setcategory(e.target.value)}
                />

            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={() => props.toggleModal()}>
                    Cancel
                </Button>
                <Button color="primary" onClick={() => handleUpdateProduct()}>
                   Update Product
                </Button>
            </ModalFooter>
        </Modal>


    );
};

function mapStateToProps(state) {
    const {selectedProducts} = state.product;
    const {users}=state.user;
    return {
        selectedProducts,
        users
    }
}

export default connect(mapStateToProps, {updateproduct,getallusers})(UpdateProductModal);
