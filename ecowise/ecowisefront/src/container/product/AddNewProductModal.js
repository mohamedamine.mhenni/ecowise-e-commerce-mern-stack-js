import React, {useEffect, useState} from 'react';

import {connect} from "react-redux";
import {addnewproduct} from "../../redux/product/actions";
import {Button, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {dropzoneComponentConfig, dropzoneConfig} from "../../components/common/dropzoneConfig";
import DropzoneComponent from "react-dropzone-component";
import axios from "axios"
const AddNewProductModal = props => {

    const [title, settitle] = useState("");
    const [owner, setowner] = useState("5f8312aaf098c82194bf7960");
    const [description, setdescription] = useState("");
    const [category, setcategory] = useState("5f873a690c335115f07084ab");
    const [price, setprice] = useState(0);
    const [pictures, setpictures] = useState([]);

     function addNewProduct() {
       props.addnewproduct(title, owner, description, category,price,pictures);
    }

    var eventHandlers = {
        addedfile: (file) => {
            pictures.push(file);console.log(file);
        },
        removedfile: (file) => {
            setpictures(pictures.filter(p => p.upload.uuid !== file.upload.uuid));
        }
    };
    return (

        <Modal
            isOpen={props.openModal}
            toggle={props.toggleModal}
            wrapClassName="modal-right"
            backdrop="static"
        >
            <ModalHeader>
                Add New Product
            </ModalHeader>
            <ModalBody>
                <Label>Title</Label>
                <Input type="text"
                       defaultValue={title}
                       onChange={e => settitle(e.target.value)}
                />
                <Label>Owner</Label>
                <Input type="text"
                       defaultValue={owner}
                       onChange={e => setowner(e.target.value)}
                />
                <Label>Category</Label>
                <Input type="text"
                       defaultValue={category}
                       onChange={e => setcategory(e.target.value)}
                />
                <Label>Price</Label>
                <Input type="number"
                       defaultValue={price}
                       onChange={e => setprice(e.target.value)}
                />
                <Label>Pictures</Label>
                <DropzoneComponent
                    config={dropzoneComponentConfig}
                    eventHandlers={eventHandlers}
                    djsConfig={dropzoneConfig}
                />
                <Label>Description</Label>
                <Input type="text"
                       defaultValue={description}
                       onChange={e => setdescription(e.target.value)}
                />

            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={() => props.toggleModal()}>
                    Cancel
                </Button>
                <Button color="primary" onClick={() => addNewProduct()}>
                    Add New Product
                </Button>
                <img src="data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7"/>
            </ModalFooter>
        </Modal>


    );
};


export default connect(null, {addnewproduct})(AddNewProductModal);
