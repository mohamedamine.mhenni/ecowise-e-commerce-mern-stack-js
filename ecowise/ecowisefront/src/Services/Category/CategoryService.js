import React from 'react';
import axios from "axios";
const apiUrl="/categories";
class CategoryService  {
    async getAllcategories(){
        return await axios.get(`${apiUrl}/Allcategories`).then((response)=>{
            return response.data;
        });
    }

    async addNewCategory(title,description,parent){
        return await axios.post(`${apiUrl}/addCategory`,{
            title: title,
            description: description,
            parent:!parent?null:parent
        }).then((response)=>{
            return response.data;
        });
    }
    async deleteCategory(categoryId){
        return await axios.delete(`${apiUrl}/deleteCategory/${categoryId}`).then((response)=>{
            return response.data;
        });
    }

    async updateCategory(id,title,description,parent){
        return await axios.patch(`${apiUrl}/updateCategory/${id}`,{
            title: title,
            description: description,
            parent:!parent?null:parent
        }).then((response)=>{
            return response.data;
        });
    }

}

export default new CategoryService();
