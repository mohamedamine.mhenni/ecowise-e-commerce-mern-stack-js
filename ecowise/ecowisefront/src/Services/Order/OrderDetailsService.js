import axios from 'axios';
const apiUrl="/orderDetails";
class OrderDetailsService {
    async getAllOrderDetails(orderId){
        return await axios.get(`${apiUrl}/AllordersDetails/${orderId}`).then((response)=>{
            return response.data;
        });
    }

}
export default new OrderDetailsService();
