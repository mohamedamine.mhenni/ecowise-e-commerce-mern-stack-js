import axios from 'axios';
const apiUrl="/orders";
class OrderService {
    async getAllOrder(){
        return await axios.get(`${apiUrl}/Allorders`).then((response)=>{
            return response.data;
        });
    }
    async addNewOrder(owner,ordredproducts,status,total){
        console.log("add order from Order/Service");
        return await axios.post(`${apiUrl}/addOrder`,{
            costumerId: owner,
            Ordredproducts:ordredproducts,
            status: status,
            total:total
        }).then((response)=>{
            return response.data;
        });
    }
    async deleteOrder(orderId){
        return await axios.delete(`${apiUrl}/deleteOrder/${orderId}`).then((response)=>{
            return response.data;
        });
    }
    async updateOrder(id,owner,ordredproducts,status,total){
        return await axios.patch(`${apiUrl}/updateOrder/${id}`,{
            costumerId: owner,
            Ordredproducts:ordredproducts,
            status: status,
            total:total
        }).then((response)=>{
            return response.data;
        });
    }
    async getOrderBy(id){
        return await axios.get(`${apiUrl}/findById/${id}`).then((response)=>{
            return response.data;
        });
    }
}
export default new OrderService();
