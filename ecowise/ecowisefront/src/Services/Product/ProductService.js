import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
const apiUrl = "/products";

class ProductService {
    async getAllproducts() {
        return await axios.get(`${apiUrl}/Allproducts`).then((response) => {
            return response.data;
        });
    }


    async addNewProduct(title, owner, description, category, price, pictures) {

        try {
            console.log(`pictures number : ${pictures.length}`);
            const formData = new FormData();
            pictures.forEach(picture => {
                let name=uuidv4();
                formData.append('files', picture, name);
            });

            formData.append('title', title);
            formData.append('owner', owner);
            formData.append('description', description);
            formData.append('category', category);
            formData.append('price', price);



            return await axios.post(`${apiUrl}/addProduct`, formData).then((response) => {
                return response.data;
            });
        } catch (e) {
            console.log(e);

        }
    }

    async deleteProduct(productId) {
        return await axios.delete(`${apiUrl}/deleteProduct/${productId}`).then((response) => {
            return response.data;
        });
    }

    async updateProduct(id, title, owner, description, category, price, pictures) {
        return await axios.patch(`${apiUrl}/updateProduct/${id}`, {
            title: title,
            owner: owner,
            description: description,
            category: category,
            price: price,
            pictures: pictures
        }).then((response) => {
            return response.data;
        });
    }

    async getproductById(productId) {
        return await axios.get(`${apiUrl}/getById/${productId}`).then((response) => {
            return response.data;
        });
    }
}
export default new ProductService();
