import axios from 'axios';
const apiUrl="/users";
class UserService {
   async getAllUsers(orderBy,search){
        return await axios.get(`${apiUrl}/AllUsers?orderBy=${orderBy}&search=${search}`).then((response)=>{
            console.log(response.data);
            return response.data;
        });
    }
    async addNewUser(firstname,lastname,username,password,email,adress,role){
        return await axios.post(`${apiUrl}/adduser`,{
            firstname:firstname,
            lastname:lastname,
            username: username,
            password: password,
            email:email,
            adress:adress,
            role:role
        }).then((response)=>{
            return response.data;
        });
    }
    async deleteUser(userId){
        return await axios.delete(`${apiUrl}/deleteUser/${userId}`).then((response)=>{
            return response.data;
        });
    }

    async updateUser(id,firstname,lastname,username,password,email,adress,role){
        return await axios.patch(`${apiUrl}/updateUser/${id}`,{
            firstname:firstname,
            lastname:lastname,
            username: username,
            password: password,
            email:email,
            adress:adress,
            role:role
        }).then((response)=>{
            return response.data;
        });
    }

}
export default new UserService();
