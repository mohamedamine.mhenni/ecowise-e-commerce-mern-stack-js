import axios from 'axios';
const apiUrl ="/authentication";
class AuthService {
    async login(email,password){
        return await axios.post(`${apiUrl}/login`,{email: email,
            password: password}).then((response)=>{
            localStorage.setItem('token', response.data.accessToken);
            localStorage.setItem('refresh_token', response.data.refreshToken);
            return response.data;
        });
        //console.log(data);

    }

     async registration(firstname,lastname,username,country,city,companyname,companydescription,phone,password,email,adress,role){
        return  await axios.post(`${apiUrl}/registration`,{
            firstname:firstname,
            lastname:lastname,
            username:username,
            password:password,
            companyname:companyname,
            companydescription:companydescription,
            country:country,
            city:city,
            phone:phone,
            email:email,
            adress:adress,
            role:role
        }).then((response)=>{
                return response.data;
        });
    }
     logout(){
        return  axios.post(`${apiUrl}/logout`,{token:localStorage.getItem('token')}).then((response)=>{
            localStorage.removeItem('token');
            localStorage.removeItem('refresh_token');
            return response.data;
        });

    }


}

export default new AuthService();
